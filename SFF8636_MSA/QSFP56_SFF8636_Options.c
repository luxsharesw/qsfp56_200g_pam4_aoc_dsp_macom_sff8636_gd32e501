#include "gd32e501.h"
#include "core_cm33.h"
#include "string.h"
#include "systick.h"
#include <stdint.h>
#include "Calibration_Struct.h"
#include "Calibration_Struct_2.h"
#include "Master_I2C1_PB3_PB4.h"
#include "BRCM_DSP_87540.h"
#include "SFF8636_MSA.h"
#include "GD_Flash_Map_SFF8636.h"

#define High_rate_Pam4_50     1
#define Low_Rate_NRZ_25       0

uint8_t Rate_Select_Change_flag =0;
uint16_t bef_Rate_Select_Value = 0;

void DSP_Rate_Select_Function(uint8_t Change_Mode)
{
    if(Change_Mode)
        DSP_MODE_SET = Mode4_1x53G_PAM4_1x53G_PAM4_4PORT;
    else
        DSP_MODE_SET = Mode3_4x25G_NRZ_4x25G_NRZ_1PORT;
    
    // Disable I2C
    i2c_disable(I2C0);
    SET_CHIP_MODE(DSP_MODE_SET);
    // Enable I2C
    i2c_enable(I2C0); 
}

void MSA_Rate_Select_Control()
{
    uint16_t Rate_Select_Control_Valus=0;
    uint8_t Temp_Buffer[128];
    
    Rate_Select_Control_Valus = (uint16_t)(SFF8636_A0[87] << 8) + (uint16_t)SFF8636_A0[88] ;
       
    if(Rate_Select_Change_flag==0)
    {
        if(Rate_Select_Control_Valus!=bef_Rate_Select_Value)
            Rate_Select_Change_flag = 1;
        bef_Rate_Select_Value = Rate_Select_Control_Valus;
    }
    else
    {
        if(Rate_Select_Control_Valus==0xAAAA)  // NRZ mode
        {
            Signal_Status=NRZ_to_NRZ;
            Get_NRZ_Default_System_Side();
            DSP_Rate_Select_Function(0x00);
        }
        if(Rate_Select_Control_Valus==0xFFFF)  // PAM mode
        {
            // DSP Line-side & System-side CH0 - CH3
            GDMCU_FMC_READ_FUNCTION( FS_DSP_LS_P86        , &Temp_Buffer[0] , 128 );
            memcpy(  &BRCM_87540_DSP_LS_PHY0_MEMORY_MAP   , &Temp_Buffer[0] , 128 );
            GDMCU_FMC_READ_FUNCTION( FS_DSP_SS_P87        , &Temp_Buffer[0] , 128 );
            memcpy(  &BRCM_87540_DSP_SS_PHY0_MEMORY_MAP   , &Temp_Buffer[0] , 128 );
            Signal_Status=PAM4_to_PAM4;
            DSP_Rate_Select_Function(0x01);
        }
        
        Rate_Select_Change_flag = 0;
    }
}

void Module_Soft_reset_Funciton()
{
    if( SFF8636_A0[93] & 0x80 )
        __NVIC_SystemReset();
}

