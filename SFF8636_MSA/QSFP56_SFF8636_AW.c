#include "gd32e501.h"
#include "SFF8636_MSA.h"
#include <stdint.h>
#include "Calibration_Struct.h"
#include "Calibration_Struct_2.h"
#include "MCU_GPIO_Customize_Define.h"

uint8_t LOS_LATCH = 0;
uint8_t TxFault_LATCH = 0;
uint8_t LOL_LATCH = 0;

uint8_t Temperature_LATCH = 0;
uint8_t VCC_LATCH = 0;
uint8_t RxPower12_LATCH = 0;
uint8_t RxPower34_LATCH = 0;
uint8_t Bias12_LATCH = 0;
uint8_t Bias34_LATCH = 0;
uint8_t TxPower12_LATCH = 0;
uint8_t TxPower34_LATCH = 0;

uint8_t Bef_SOFT_DIS_STATUS = 0;
uint8_t Bef_CDR_STATUS = 0x01;
uint16_t IntL_Status = 0;

void QSFP28_Temperature_AW()
{
	int16_t T_Temp;
	int16_t High_Alarm_Value, Low_Alarm_Value, High_Warning, Low_Warning;
	uint8_t LATCH = 0;
	uint8_t MASK_FLAG;

	T_Temp           = (int16_t) (SFF8636_A0[22] << 8)   | SFF8636_A0[23];
	High_Alarm_Value = (int16_t) (SFF8636_PAGE3[0] << 8) | SFF8636_PAGE3[1];
	Low_Alarm_Value  = (int16_t) (SFF8636_PAGE3[2] << 8) | SFF8636_PAGE3[3];
	High_Warning     = (int16_t) (SFF8636_PAGE3[4] << 8) | SFF8636_PAGE3[5];
	Low_Warning      = (int16_t) (SFF8636_PAGE3[6] << 8) | SFF8636_PAGE3[7];

	MASK_FLAG = SFF8636_A0[103];

	if (T_Temp < Low_Warning) {
		LATCH |= 0x10;		          // set high alarm flag bit4
	}
	if (T_Temp > High_Warning) {
		LATCH |= 0x20;		          // set high alarm flag bit5
	}
	if (T_Temp < Low_Alarm_Value) {
		LATCH |= 0x40;		          // set high alarm flag bit6
	}
	if (T_Temp > High_Alarm_Value) {
		LATCH |= 0x80;		          // set high alarm flag bit7
	}

	if ((SFF8636_A0[6] > 0) && (LATCH == 0)) {
		LATCH = SFF8636_A0[6];
	}

	Temperature_LATCH = LATCH;

	SFF8636_A0[6] |= LATCH;

	if ((LATCH) & (~MASK_FLAG)) 
    {
		IntL_G_Low();
        
		SFF8636_A0[2] &= ~0x02;
		IntL_Status |= TEMP_INT;
	}
}

void QSFP28_VCC_AW() 
{
	uint16_t VCC_Temp;
	uint16_t High_Alarm_Value, Low_Alarm_Value, High_Warning, Low_Warning;
	uint8_t LATCH = 0;
	uint8_t MASK_FLAG;

	VCC_Temp         = (uint16_t) (SFF8636_A0[26] << 8)    | SFF8636_A0[27];
	High_Alarm_Value = (uint16_t) (SFF8636_PAGE3[16] << 8) | SFF8636_PAGE3[17];
	Low_Alarm_Value  = (uint16_t) (SFF8636_PAGE3[18] << 8) | SFF8636_PAGE3[19];
	High_Warning     = (uint16_t) (SFF8636_PAGE3[20] << 8) | SFF8636_PAGE3[21];
	Low_Warning      = (uint16_t) (SFF8636_PAGE3[22] << 8) | SFF8636_PAGE3[23];

	MASK_FLAG = SFF8636_A0[104];

	if (VCC_Temp < Low_Warning) 
		LATCH |= 0x10;		          // set high alarm flag bit4
	
	if (VCC_Temp > High_Warning)
        LATCH |= 0x20;		          // set high alarm flag bit5
	
	if (VCC_Temp < Low_Alarm_Value) 
		LATCH |= 0x40;		          // set high alarm flag bit6
	
	if (VCC_Temp > High_Alarm_Value) 
		LATCH |= 0x80;		          // set high alarm flag bit7

	if ((SFF8636_A0[7] > 0) && (LATCH == 0))
		LATCH = SFF8636_A0[7];

	VCC_LATCH = LATCH;

	SFF8636_A0[7] = LATCH;

	if ((LATCH) & (~MASK_FLAG)) 
    {
		IntL_G_Low();
        
		SFF8636_A0[2] &= ~0x02;
		IntL_Status |= VCC_INT;
	}
}

void QSFP28_RxPower_AW() 
{
	uint16_t RXPOWER0_Temp, RXPOWER1_Temp, RXPOWER2_Temp, RXPOWER3_Temp;
	uint16_t High_Alarm_Value, Low_Alarm_Value, High_Warning, Low_Warning;
	uint8_t LATCH = 0;
	uint8_t LATCH2 = 0;

	RXPOWER0_Temp = (uint16_t) (SFF8636_A0[34] << 8) | SFF8636_A0[35];
	RXPOWER1_Temp = (uint16_t) (SFF8636_A0[36] << 8) | SFF8636_A0[37];
	RXPOWER2_Temp = (uint16_t) (SFF8636_A0[38] << 8) | SFF8636_A0[39];
	RXPOWER3_Temp = (uint16_t) (SFF8636_A0[40] << 8) | SFF8636_A0[41];

	High_Alarm_Value = (uint16_t) (SFF8636_PAGE3[48] << 8) | SFF8636_PAGE3[49];
	Low_Alarm_Value  = (uint16_t) (SFF8636_PAGE3[50] << 8) | SFF8636_PAGE3[51];
	High_Warning     = (uint16_t) (SFF8636_PAGE3[52] << 8) | SFF8636_PAGE3[53];
	Low_Warning      = (uint16_t) (SFF8636_PAGE3[54] << 8) | SFF8636_PAGE3[55];
	//-------------------------------------------------------------//
	// RxPower Channel 1
	//-------------------------------------------------------------//
	if (RXPOWER0_Temp < Low_Warning) 
		LATCH |= 0x10;		          // set high alarm flag bit4
	
	if (RXPOWER0_Temp > High_Warning)
		LATCH |= 0x20;		          // set high alarm flag bit5

	if (RXPOWER0_Temp < Low_Alarm_Value)
		LATCH |= 0x40;		          // set high alarm flag bit6
	
	if (RXPOWER0_Temp > High_Alarm_Value) 
		LATCH |= 0x80;		          // set high alarm flag bit7
	//-------------------------------------------------------------//
	// RxPower Channel 2
	//-------------------------------------------------------------//
	if (RXPOWER1_Temp < Low_Warning)
		LATCH |= 0x01;		          // set high alarm flag bit0
	if (RXPOWER1_Temp > High_Warning) 
		LATCH |= 0x02;		          // set high alarm flag bit1
	if (RXPOWER1_Temp < Low_Alarm_Value)
		LATCH |= 0x04;		          // set high alarm flag bit2
	if (RXPOWER1_Temp > High_Alarm_Value) 
		LATCH |= 0x08;		          // set high alarm flag bit3
	//-------------------------------------------------------------//
	// RxPower Channel 3
	//-------------------------------------------------------------//
	if (RXPOWER2_Temp < Low_Warning)
		LATCH2 |= 0x10;		          // set high alarm flag bit4
	if (RXPOWER2_Temp > High_Warning)
		LATCH2 |= 0x20;		          // set high alarm flag bit5
	if (RXPOWER2_Temp < Low_Alarm_Value)
		LATCH2 |= 0x40;		          // set high alarm flag bit6
	if (RXPOWER2_Temp > High_Alarm_Value)
		LATCH2 |= 0x80;		          // set high alarm flag bit7
	//-------------------------------------------------------------//
	// RxPower Channel 4
	//-------------------------------------------------------------//
	if (RXPOWER3_Temp < Low_Warning)
		LATCH2 |= 0x01;		          // set high alarm flag bit0
	if (RXPOWER3_Temp > High_Warning) 
		LATCH2 |= 0x02;		          // set high alarm flag bit1
	if (RXPOWER3_Temp < Low_Alarm_Value) 
		LATCH2 |= 0x04;		          // set high alarm flag bit2
	if (RXPOWER3_Temp > High_Alarm_Value) 
		LATCH2 |= 0x08;		          // set high alarm flag bit3

	if ((SFF8636_A0[9] > 0) && (LATCH == 0))
		LATCH = SFF8636_A0[9];

	if ((SFF8636_A0[10] > 0) && (LATCH2 == 0))
		LATCH2 = SFF8636_A0[10];

	RxPower12_LATCH = LATCH;
	SFF8636_A0[9] = LATCH;

	RxPower34_LATCH = LATCH2;
	SFF8636_A0[10] = LATCH2;

	if (LATCH & (~SFF8636_PAGE3[114]))
	{
		IntL_G_Low();
        
		SFF8636_A0[2] &= ~0x02;
		IntL_Status |= RX12_INT;
	}
	if (LATCH2 & (~SFF8636_PAGE3[115]))
	{
		IntL_G_Low();
        
		SFF8636_A0[2] &= ~0x02;
		IntL_Status |= RX34_INT;
	}
}

void QSFP28_Bias_AW() 
{
	uint16_t Bias0_Temp, Bias1_Temp, Bias2_Temp, Bias3_Temp;
	uint16_t High_Alarm_Value, Low_Alarm_Value, High_Warning, Low_Warning;
	uint8_t LATCH = 0, LATCH2 = 0;

	Bias0_Temp = (uint16_t) (SFF8636_A0[42] << 8) | SFF8636_A0[43];
	Bias1_Temp = (uint16_t) (SFF8636_A0[44] << 8) | SFF8636_A0[45];
	Bias2_Temp = (uint16_t) (SFF8636_A0[46] << 8) | SFF8636_A0[47];
	Bias3_Temp = (uint16_t) (SFF8636_A0[48] << 8) | SFF8636_A0[49];

	High_Alarm_Value = (uint16_t) (SFF8636_PAGE3[56] << 8) | SFF8636_PAGE3[57];
	Low_Alarm_Value  = (uint16_t) (SFF8636_PAGE3[58] << 8) | SFF8636_PAGE3[59];
	High_Warning     = (uint16_t) (SFF8636_PAGE3[60] << 8) | SFF8636_PAGE3[61];
	Low_Warning      = (uint16_t) (SFF8636_PAGE3[62] << 8) | SFF8636_PAGE3[63];
	//-------------------------------------------------------------//
	// Bias Channel 1
	//-------------------------------------------------------------//
	if (Bias0_Temp < Low_Warning)
		LATCH |= 0x10;		          // set high alarm flag bit4
	
	if (Bias0_Temp > High_Warning)
		LATCH |= 0x20;		          // set high alarm flag bit5
	
	if (Bias0_Temp < Low_Alarm_Value)
		LATCH |= 0x40;		          // set high alarm flag bit6
	
	if (Bias0_Temp > High_Alarm_Value)
		LATCH |= 0x80;		          // set high alarm flag bit7
	
	//-------------------------------------------------------------//
	// Bias Channel 2
	//-------------------------------------------------------------//
	if (Bias1_Temp < Low_Warning)
        LATCH |= 0x01;		          // set high alarm flag bit0
	
	if (Bias1_Temp > High_Warning)
		LATCH |= 0x02;		          // set high alarm flag bit1
	
	if (Bias1_Temp < Low_Alarm_Value)
		LATCH |= 0x04;		          // set high alarm flag bit2
	
	if (Bias1_Temp > High_Alarm_Value)
		LATCH |= 0x08;		          // set high alarm flag bit3
	
	//-------------------------------------------------------------//
	// Bias Channel 3
	//-------------------------------------------------------------//
	if (Bias2_Temp < Low_Warning)
		LATCH2 |= 0x10;		          // set high alarm flag bit4
	
	if (Bias2_Temp > High_Warning)
		LATCH2 |= 0x20;		          // set high alarm flag bit5
	
	if (Bias2_Temp < Low_Alarm_Value)
		LATCH2 |= 0x40;		          // set high alarm flag bit6
	
	if (Bias2_Temp > High_Alarm_Value)
		LATCH2 |= 0x80;		          // set high alarm flag bit7
	
	//-------------------------------------------------------------//
	// Bias Channel 4
	//-------------------------------------------------------------//
	if (Bias3_Temp < Low_Warning)
		LATCH2 |= 0x01;		          // set high alarm flag bit0
	
	if (Bias3_Temp > High_Warning)
		LATCH2 |= 0x02;		          // set high alarm flag bit1
	
	if (Bias3_Temp < Low_Alarm_Value)
		LATCH2 |= 0x04;		          // set high alarm flag bit2
	
	if (Bias3_Temp > High_Alarm_Value)
		LATCH2 |= 0x08;		          // set high alarm flag bit3
	

	if ((SFF8636_A0[11] > 0) && (LATCH == 0))
		LATCH = SFF8636_A0[11];

	if ((SFF8636_A0[12] > 0) && (LATCH2 == 0))
		LATCH2 = SFF8636_A0[12];

	Bias12_LATCH = LATCH;
	SFF8636_A0[11] = LATCH;

	Bias34_LATCH = LATCH2;
	SFF8636_A0[12] = LATCH2;

	if (LATCH & (~SFF8636_PAGE3[116])) 
    {
		IntL_G_Low();
        
		SFF8636_A0[2] &= ~0x02;
		IntL_Status |= TXB12_INT;
	}
	if (LATCH2 & (~SFF8636_PAGE3[117])) 
    {
		IntL_G_Low();
        
		SFF8636_A0[2] &= ~0x02;
		IntL_Status |= TXB34_INT;
	}
}

void QSFP28_TxPower_AW() 
{
	uint16_t TXPOWER0_Temp, TXPOWER1_Temp, TXPOWER2_Temp, TXPOWER3_Temp;
	uint16_t High_Alarm_Value, Low_Alarm_Value, High_Warning, Low_Warning;
	uint8_t LATCH = 0, LATCH2 = 0;

	TXPOWER0_Temp = (uint16_t) (SFF8636_A0[50] << 8) | SFF8636_A0[51];
	TXPOWER1_Temp = (uint16_t) (SFF8636_A0[52] << 8) | SFF8636_A0[53];
	TXPOWER2_Temp = (uint16_t) (SFF8636_A0[54] << 8) | SFF8636_A0[55];
	TXPOWER3_Temp = (uint16_t) (SFF8636_A0[56] << 8) | SFF8636_A0[57];

	High_Alarm_Value = (uint16_t) (SFF8636_PAGE3[64] << 8) | SFF8636_PAGE3[65];
	Low_Alarm_Value  = (uint16_t) (SFF8636_PAGE3[66] << 8) | SFF8636_PAGE3[67];
	High_Warning     = (uint16_t) (SFF8636_PAGE3[68] << 8) | SFF8636_PAGE3[69];
	Low_Warning      = (uint16_t) (SFF8636_PAGE3[70] << 8) | SFF8636_PAGE3[71];
	//-------------------------------------------------------------//
	// TxPower Channel 1
	//-------------------------------------------------------------//
	if (TXPOWER0_Temp < Low_Warning)
		LATCH |= 0x10;		          // set high alarm flag bit4
    
	if (TXPOWER0_Temp > High_Warning)
		LATCH |= 0x20;		          // set high alarm flag bit5
	
	if (TXPOWER0_Temp < Low_Alarm_Value)
		LATCH |= 0x40;		          // set high alarm flag bit6
	
	if (TXPOWER0_Temp > High_Alarm_Value)
		LATCH |= 0x80;		          // set high alarm flag bit7
	//-------------------------------------------------------------//
	// TxPower Channel 2
	//-------------------------------------------------------------//
	if (TXPOWER1_Temp < Low_Warning)
		LATCH |= 0x01;		          // set high alarm flag bit0

	if (TXPOWER1_Temp > High_Warning)
		LATCH |= 0x02;		          // set high alarm flag bit1
	
	if (TXPOWER1_Temp < Low_Alarm_Value)
		LATCH |= 0x04;		          // set high alarm flag bit2
	
	if (TXPOWER1_Temp > High_Alarm_Value)
		LATCH |= 0x08;		          // set high alarm flag bit3
	
	//-------------------------------------------------------------//
	// TxPower Channel 3
	//-------------------------------------------------------------//
	if (TXPOWER2_Temp < Low_Warning)
		LATCH2 |= 0x10;		          // set high alarm flag bit4
	
	if (TXPOWER2_Temp > High_Warning)
		LATCH2 |= 0x20;		          // set high alarm flag bit5
	
	if (TXPOWER2_Temp < Low_Alarm_Value)
		LATCH2 |= 0x40;		          // set high alarm flag bit6
	
	if (TXPOWER2_Temp > High_Alarm_Value)
		LATCH2 |= 0x80;		          // set high alarm flag bit7
	//-------------------------------------------------------------//
	// TxPower Channel 4
	//-------------------------------------------------------------//
	if (TXPOWER3_Temp < Low_Warning)
		LATCH2 |= 0x01;		          // set high alarm flag bit0

	if (TXPOWER3_Temp > High_Warning)
		LATCH2 |= 0x02;		          // set high alarm flag bit1
	
	if (TXPOWER3_Temp < Low_Alarm_Value)
		LATCH2 |= 0x04;		          // set high alarm flag bit2
	
	if (TXPOWER3_Temp > High_Alarm_Value)
		LATCH2 |= 0x08;		          // set high alarm flag bit3
	

	if ((SFF8636_A0[13] > 0) && (LATCH == 0))
		LATCH = SFF8636_A0[13];

	if ((SFF8636_A0[14] > 0) && (LATCH2 == 0))
		LATCH2 = SFF8636_A0[14];

	TxPower12_LATCH = LATCH;
	SFF8636_A0[13] = LATCH;

	TxPower34_LATCH = LATCH2;
	SFF8636_A0[14] = LATCH2;

	if (LATCH & (~SFF8636_PAGE3[118])) 
    {
		IntL_G_Low();
        
		SFF8636_A0[2] &= ~0x02;
		IntL_Status |= TXB12_INT;
	}
	if (LATCH2 & (~SFF8636_PAGE3[119])) 
    {
		IntL_G_Low();
        
		SFF8636_A0[2] &= ~0x02;
		IntL_Status |= TXB34_INT;
	}
}

void Clear_Flag(uint8_t Current_AD)
{
	switch (Current_AD)
	{
		case ADDR_LOS:
			SFF8636_A0[3] = 0;
			//IntL_Status &= ~LOS_INT;
			break;
		case ADDR_TXFAULT:
			SFF8636_A0[4] = 0;
			IntL_Status &= ~FAULT_INT;
			break;
		case ADDR_CDR_LOL:
			SFF8636_A0[5] &= ~LOL_LATCH;
			IntL_Status &= ~CDR_LOL_INT;
			break;
		case ADDR_TEMP:
			SFF8636_A0[6] = 0;
			IntL_Status &= ~TEMP_INT;
			break;
		case ADDR_VCC:
			SFF8636_A0[7] &= ~VCC_LATCH;
			IntL_Status &= ~VCC_INT;
			break;
		case ADDR_RXP12:
			SFF8636_A0[9] &= ~RxPower12_LATCH;
			IntL_Status &= ~RX12_INT;
			break;
		case ADDR_RXP34:
			SFF8636_A0[10] &= ~RxPower34_LATCH;
			IntL_Status &= ~RX34_INT;
			break;
		case ADDR_TXB12:
			SFF8636_A0[11] = 0x00 ;

			break;
		case ADDR_TXB34:
			SFF8636_A0[12] = 0x00 ;

			break;
		case ADDR_TXP12:
			SFF8636_A0[13] = 0x00 ;

			break;
		case ADDR_TXP34:
			SFF8636_A0[14] = 0x00 ;

			break;
	}
	if (IntL_Status == 0x00)
	{
		SFF8636_A0[2] |= 0x02;
		IntL_G_High();
	}
}

void SFF8636_DDMI_AW()
{
	QSFP28_Temperature_AW();
	QSFP28_VCC_AW();
	QSFP28_RxPower_AW();
}



