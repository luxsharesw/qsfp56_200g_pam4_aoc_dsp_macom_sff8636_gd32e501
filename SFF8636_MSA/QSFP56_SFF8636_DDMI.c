#include "gd32e501.h"
#include <stdint.h>
#include "SFF8636_MSA.h"
#include "MA38435_TX1_TX4.h"
#include "MA38434_RX1_RX4.h"
#include "Calibration_Struct.h"
#include "Calibration_Struct_2.h"
#include "MCU_GPIO_Customize_Define.h"

#define ADC_SM              0
#define TXB_TXP_01_SM       1
#define TXB_TXP_23_SM       2
#define RXP_01_SM           3
#define RXP_23_SM           4
#define OP_SM               5
#define Other_DDMI          6

#define SHOW_RSSI_Enable   1
#define SHOW_RSSI_Disable  0

int8_t Temperature_Index = 0 ;
uint8_t Bef_ForceMute_CH0_CH3 = 0 ;
uint8_t Bef_ForceMute_CH4_CH7 = 0 ;
uint8_t SHOW_RSSI_CH0 = 1 ;
uint8_t SHOW_RSSI_CH1 = 1 ;
uint8_t SHOW_RSSI_CH2 = 1 ;
uint8_t SHOW_RSSI_CH3 = 1 ;

uint16_t Bias_Current_CH0,Bias_Current_CH1,Bias_Current_CH2,Bias_Current_CH3;
uint16_t Bias_Current_CH4,Bias_Current_CH5,Bias_Current_CH6,Bias_Current_CH7;

extern uint8_t Power_on_flag;

int16_t Temp_CAL( int16_t Vaule , uint8_t Slop , uint8_t Shift , int16_t Offset)
{
	uint32_t Buffer_temp ;

	Buffer_temp = (uint32_t)Vaule * Slop ;
	Buffer_temp = (uint32_t)Buffer_temp >> Shift ;

	Vaule = (uint32_t)Buffer_temp + Offset * 256 ;

    return Vaule ;
}

uint16_t CAL_Function( uint16_t Vaule , uint8_t Slop , uint8_t Shift , int16_t Offset )
{
	uint32_t Buffer_temp ;

	Buffer_temp = (uint32_t)Vaule * Slop ;
	Buffer_temp = (uint32_t)Buffer_temp >> Shift ;

	Vaule = (uint32_t)Buffer_temp + Offset;

	return Vaule;
}

void Temperature_Monitor(int16_t tempsensor_value)
{
	int16_t Temp_Buffer;
	int8_t Indx_Temp ;
	Temp_Buffer = Temp_CAL(tempsensor_value,CALIB_MEMORY_MAP.TEMP_SCALE1M,CALIB_MEMORY_MAP.TEMP_SCALE1L,CALIB_MEMORY_MAP.TEMP_OFFSET1);

	SFF8636_A0[22] = Temp_Buffer >> 8 ;
	SFF8636_A0[23] = Temp_Buffer ;

	Indx_Temp = SFF8636_A0[22];

    if(Indx_Temp<-40)
        Indx_Temp = -40 ;

    if(Indx_Temp>85)
        Indx_Temp = 86 ;

	Temperature_Index = (Indx_Temp + 40) /2;
}

void VCC_Monitor(uint16_t VCC_Vaule)
{
    int16_t  offset_Buffer_VCC = 0 ;
    offset_Buffer_VCC = (CALIB_MEMORY_MAP.VCC_OFFSET_MSB << 8 | CALIB_MEMORY_MAP.VCC_OFFSET_LSB );
    
	VCC_Vaule = CAL_Function( VCC_Vaule , CALIB_MEMORY_MAP.VCC_SCALEM , CALIB_MEMORY_MAP.VCC_SCALEL , offset_Buffer_VCC ) ;
    
	SFF8636_A0[26] = VCC_Vaule >> 8 ;
	SFF8636_A0[27] = VCC_Vaule ;
}

void BIAS_TxPower_Monitor( uint8_t Channel)
{
	uint16_t MOD_Temp_Buffer = 0;
	uint16_t TXP_BUFFER = 0 ;  
    int16_t  offset_Buffer_B = 0 ;
    int16_t  offset_Buffer_P = 0 ;

	switch(Channel)
	{
		case 0:
				Bias_Current_CH0 = TXBIAS_TX1_TX4(0x00) ;
                // Vcsel Driver Max Bias is 15.2mA set max limit to 16.384mA=>0x2000 and min limit to 2.048mA
                if((Bias_Current_CH0>0x2000)||(Bias_Current_CH0<0x0400))
					Bias_Current_CH0 = (uint16_t) (SFF8636_A0[42] << 8) | SFF8636_A0[43];
                        
				TXP_BUFFER = Bias_Current_CH0 ;
        
                offset_Buffer_B = (CALIB_MEMORY_MAP.IBias0_OFFSET_MSB << 8 | CALIB_MEMORY_MAP.IBias0_OFFSET_LSB );
                offset_Buffer_P = (CALIB_MEMORY_MAP.TXP0_OFFSET_MSB << 8 | CALIB_MEMORY_MAP.TXP0_OFFSET_LSB );
        
				Bias_Current_CH0 = CAL_Function( Bias_Current_CH0 , CALIB_MEMORY_MAP.IBias0_SCALEM , CALIB_MEMORY_MAP.IBias0_SCALE1L , offset_Buffer_B ) ;
				TXP_BUFFER = CAL_Function( TXP_BUFFER , CALIB_MEMORY_MAP.TXP0_SCALEM , CALIB_MEMORY_MAP.TXP0_SCALEL , offset_Buffer_P ) ;

				if( SFF8636_A0[86] & 0x01 )
				{
					SFF8636_A0[42] = 0 ;
					SFF8636_A0[43] = 0 ;
					// CH0  Tx Power
					SFF8636_A0[50] = 0 ;
					SFF8636_A0[51] = 0 ;
					// 2019-0618 Ivan Modify
					// Low Alarm & Low Waring
					// Tx Bias
					SFF8636_A0[11] |= 0x50 ;
					// Tx Power
					SFF8636_A0[13] |= 0x50 ;
				}
				else
				{
					SFF8636_A0[42] = Bias_Current_CH0 >> 8 ;
					SFF8636_A0[43] = Bias_Current_CH0 ;
					// CH0 TxPower
					SFF8636_A0[50] = TXP_BUFFER >> 8 ;
					SFF8636_A0[51] = TXP_BUFFER ;
				}
				break;
		case 1:
				Bias_Current_CH1 = TXBIAS_TX1_TX4(0x20) ;
                // Vcsel Driver Max Bias is 15.2mA set max limit to 16.384mA=>0x2000 and min limit to 2.048mA
                if((Bias_Current_CH1>0x2000)||(Bias_Current_CH1<0x0400))
					Bias_Current_CH1 = (uint16_t) (SFF8636_A0[44] << 8) | SFF8636_A0[45];
				TXP_BUFFER = Bias_Current_CH1 ;
        
                offset_Buffer_B = (CALIB_MEMORY_MAP.IBias1_OFFSET_MSB << 8 | CALIB_MEMORY_MAP.IBias1_OFFSET_LSB );
                offset_Buffer_P = (CALIB_MEMORY_MAP.TXP1_OFFSET_MSB << 8 | CALIB_MEMORY_MAP.TXP1_OFFSET_LSB );

				Bias_Current_CH1 = CAL_Function( Bias_Current_CH1 , CALIB_MEMORY_MAP.IBias1_SCALEM , CALIB_MEMORY_MAP.IBias1_SCALE1L , offset_Buffer_B ) ;
				TXP_BUFFER = CAL_Function( TXP_BUFFER , CALIB_MEMORY_MAP.TXP1_SCALEM , CALIB_MEMORY_MAP.TXP1_SCALEL , offset_Buffer_P ) ;

				if( SFF8636_A0[86] & 0x02 )
				{
					SFF8636_A0[44] = 0 ;
					SFF8636_A0[45] = 0 ;
					// CH1  Tx Power
					SFF8636_A0[52] = 0 ;
					SFF8636_A0[53] = 0 ;
					// 2019-0618 Ivan Modify
					// Low Alarm & Low Waring
					// Tx Bias
					SFF8636_A0[11] |= 0x05 ;
					// Tx Power
					SFF8636_A0[13] |= 0x05 ;
				}
				else
				{
					SFF8636_A0[44] = Bias_Current_CH1 >> 8 ;
					SFF8636_A0[45] = Bias_Current_CH1 ;
					// CH1  Tx Power
					SFF8636_A0[52] = TXP_BUFFER >> 8 ;
					SFF8636_A0[53] = TXP_BUFFER ;
				}
				break; 
		case 2:
				Bias_Current_CH2 = TXBIAS_TX1_TX4(0x40) ;
                // Vcsel Driver Max Bias is 15.2mA set max limit to 16.384mA=>0x2000 and min limit to 2.048mA
                if((Bias_Current_CH2>0x2000)||(Bias_Current_CH2<0x0400))
					Bias_Current_CH2 = (uint16_t) (SFF8636_A0[46] << 8) | SFF8636_A0[47];
				TXP_BUFFER = Bias_Current_CH2 ;
        
                offset_Buffer_B = (CALIB_MEMORY_MAP.IBias2_OFFSET_MSB << 8 | CALIB_MEMORY_MAP.IBias2_OFFSET_LSB );
                offset_Buffer_P = (CALIB_MEMORY_MAP.TXP2_OFFSET_MSB << 8 | CALIB_MEMORY_MAP.TXP2_OFFSET_LSB );

				Bias_Current_CH2 = CAL_Function( Bias_Current_CH2 , CALIB_MEMORY_MAP.IBias2_SCALEM , CALIB_MEMORY_MAP.IBias2_SCALE1L , offset_Buffer_B ) ;
				TXP_BUFFER = CAL_Function( TXP_BUFFER , CALIB_MEMORY_MAP.TXP2_SCALEM , CALIB_MEMORY_MAP.TXP2_SCALEL , offset_Buffer_P ) ;

				if( SFF8636_A0[86] & 0x04 )
				{
					SFF8636_A0[46] = 0 ;
					SFF8636_A0[47] = 0 ;
					// CH2  Tx Power
					SFF8636_A0[54] = 0 ;
					SFF8636_A0[55] = 0 ;
					// 2019-0618 Ivan Modify
					// Low Alarm & Low Waring
					// Tx Bias
					SFF8636_A0[12] |= 0x50 ;
					// Tx Power
					SFF8636_A0[14] |= 0x50 ;
				}
				else
				{
					SFF8636_A0[46] = Bias_Current_CH2 >> 8 ;
					SFF8636_A0[47] = Bias_Current_CH2 ;
					// CH2  Tx Power
					SFF8636_A0[54] = TXP_BUFFER >> 8 ;
					SFF8636_A0[55] = TXP_BUFFER ;
				}
			    break;
		case 3:
				Bias_Current_CH3 = TXBIAS_TX1_TX4(0x60) ;
                // Vcsel Driver Max Bias is 15.2mA set max limit to 16.384mA=>0x2000 and min limit to 2.048mA
                if((Bias_Current_CH3>0x2000)||(Bias_Current_CH3<0x0400))
					Bias_Current_CH3 = (uint16_t) (SFF8636_A0[48] << 8) | SFF8636_A0[49];
				TXP_BUFFER = Bias_Current_CH3 ;
        
                offset_Buffer_B = (CALIB_MEMORY_MAP.IBias3_OFFSET_MSB << 8 | CALIB_MEMORY_MAP.IBias3_OFFSET_LSB );
                offset_Buffer_P = (CALIB_MEMORY_MAP.TXP3_OFFSET_MSB << 8 | CALIB_MEMORY_MAP.TXP3_OFFSET_LSB );

				Bias_Current_CH3 = CAL_Function( Bias_Current_CH3 , CALIB_MEMORY_MAP.IBias3_SCALEM , CALIB_MEMORY_MAP.IBias3_SCALE1L , offset_Buffer_B ) ;
				TXP_BUFFER = CAL_Function( TXP_BUFFER , CALIB_MEMORY_MAP.TXP3_SCALEM , CALIB_MEMORY_MAP.TXP3_SCALEL , offset_Buffer_P ) ;

				if( SFF8636_A0[86] & 0x08 )
				{
					SFF8636_A0[48] = 0 ;
					SFF8636_A0[49] = 0 ;
					// CH3  Tx Power
					SFF8636_A0[56] = 0 ;
					SFF8636_A0[57] = 0 ;
					// 2019-0618 Ivan Modify
					// Low Alarm & Low Waring
					// Tx Bias
					SFF8636_A0[12] |= 0x05 ;
					// Tx Power
					SFF8636_A0[14] |= 0x05 ;
				}
				else
				{
					SFF8636_A0[48] = Bias_Current_CH3 >> 8 ;
					SFF8636_A0[49] = Bias_Current_CH3 ;
					// CH3  Tx Power
					SFF8636_A0[56] = TXP_BUFFER >> 8 ;
					SFF8636_A0[57] = TXP_BUFFER ;
				}
				break;
		default:
				break;
	}

}

void RX_POWER_M_CH0_CH4(uint8_t Rx_CH)
{
	uint16_t RX_POWER_CH0,RX_POWER_CH1,RX_POWER_CH2,RX_POWER_CH3;
	uint16_t RX_POWER_CH4,RX_POWER_CH5,RX_POWER_CH6,RX_POWER_CH7;
    
    int16_t  offset_Buffer_RXP = 0 ;
    uint16_t RXLOS_Assert_Buffer = 0 ;
    uint16_t RXLOS_DeAssert_Buffer = 0 ;

	switch(Rx_CH)
	{
		case 0:
			    RX_POWER_CH0 = GET_RSSI_CH0_CH3( 3 );
        
                offset_Buffer_RXP = (CALIB_MEMORY_MAP.RX0_OFFSET_MSB << 8 | CALIB_MEMORY_MAP.RX0_OFFSET_LSB );
                RXLOS_Assert_Buffer = (CALIB_MEMORY_MAP.Rx0_LOS_Assret_MSB << 8 | CALIB_MEMORY_MAP.Rx0_LOS_Assret_LSB  );
                RXLOS_DeAssert_Buffer = (CALIB_MEMORY_MAP.Rx0_LOS_DeAssret_MSB << 8 | CALIB_MEMORY_MAP.Rx0_LOS_DeAssret_LSB  );

			    RX_POWER_CH0 = CAL_Function( RX_POWER_CH0 , CALIB_MEMORY_MAP.RX0_SCALEM , CALIB_MEMORY_MAP.RX0_SCALEL , offset_Buffer_RXP );
			    if(RX_POWER_CH0>0xF000)
			    	RX_POWER_CH0 = 0;
				// RX RSSI CH 0
				if( RX_POWER_CH0 < RXLOS_Assert_Buffer )
					SHOW_RSSI_CH0 = SHOW_RSSI_Disable ;

				if ( RX_POWER_CH0 >= RXLOS_DeAssert_Buffer )
					SHOW_RSSI_CH0 = SHOW_RSSI_Enable ;

				if( SHOW_RSSI_CH0 == SHOW_RSSI_Enable )
				{
					SFF8636_A0[34] = RX_POWER_CH0 >> 8 ;
					SFF8636_A0[35] = RX_POWER_CH0 ;
				}
				else
				{
					SFF8636_A0[34] = 0 ;
					SFF8636_A0[35] = 1 ;

					SFF8636_A0[3] |= 0x01 ;
				}
				break;
		case 1:
			    RX_POWER_CH1 = GET_RSSI_CH0_CH3( 2 );
        
                offset_Buffer_RXP = (CALIB_MEMORY_MAP.RX1_OFFSET_MSB << 8 | CALIB_MEMORY_MAP.RX1_OFFSET_LSB );
                RXLOS_Assert_Buffer = (CALIB_MEMORY_MAP.Rx1_LOS_Assret_MSB << 8 | CALIB_MEMORY_MAP.Rx1_LOS_Assret_LSB  );
                RXLOS_DeAssert_Buffer = (CALIB_MEMORY_MAP.Rx1_LOS_DeAssret_MSB << 8 | CALIB_MEMORY_MAP.Rx1_LOS_DeAssret_LSB  );        

			    RX_POWER_CH1 = CAL_Function( RX_POWER_CH1 , CALIB_MEMORY_MAP.RX1_SCALEM , CALIB_MEMORY_MAP.RX1_SCALEL , offset_Buffer_RXP );
			    if(RX_POWER_CH1>0xF000)
			    	RX_POWER_CH1 = 0;
				// RX RSSI CH 1
				if( RX_POWER_CH1 < RXLOS_Assert_Buffer )
					SHOW_RSSI_CH1 = SHOW_RSSI_Disable;

				if( RX_POWER_CH1 >= RXLOS_DeAssert_Buffer )
					SHOW_RSSI_CH1 = SHOW_RSSI_Enable;

				if( SHOW_RSSI_CH1 == SHOW_RSSI_Enable )
				{
					SFF8636_A0[36] = RX_POWER_CH1 >> 8 ;
					SFF8636_A0[37] = RX_POWER_CH1 ;
				}
				else
				{
					SFF8636_A0[36] = 0;
					SFF8636_A0[37] = 1 ;

					SFF8636_A0[3] |= 0x02 ;
				}

				break;
		case 2:
				RX_POWER_CH2 = GET_RSSI_CH0_CH3( 1 );
                offset_Buffer_RXP = (CALIB_MEMORY_MAP.RX2_OFFSET_MSB << 8 | CALIB_MEMORY_MAP.RX2_OFFSET_LSB );
                RXLOS_Assert_Buffer = (CALIB_MEMORY_MAP.Rx2_LOS_Assret_MSB << 8 | CALIB_MEMORY_MAP.Rx2_LOS_Assret_LSB  );
                RXLOS_DeAssert_Buffer = (CALIB_MEMORY_MAP.Rx2_LOS_DeAssret_MSB << 8 | CALIB_MEMORY_MAP.Rx2_LOS_DeAssret_LSB  );             
        
				RX_POWER_CH2 = CAL_Function( RX_POWER_CH2 , CALIB_MEMORY_MAP.RX2_SCALEM , CALIB_MEMORY_MAP.RX2_SCALEL , offset_Buffer_RXP );
				if(RX_POWER_CH2>0xF000)
					RX_POWER_CH2 = 0;
				// RX RSSI CH 2
				if( RX_POWER_CH2 < RXLOS_Assert_Buffer )
					SHOW_RSSI_CH2 = SHOW_RSSI_Disable;

				if( RX_POWER_CH2 >= RXLOS_DeAssert_Buffer )
					SHOW_RSSI_CH2 = SHOW_RSSI_Enable;

				if( SHOW_RSSI_CH2 == SHOW_RSSI_Enable )
				{
					SFF8636_A0[38] = RX_POWER_CH2 >> 8 ;
					SFF8636_A0[39] = RX_POWER_CH2 ;
				}
				else
				{
					SFF8636_A0[38] = 0;
					SFF8636_A0[39] = 1 ;

					SFF8636_A0[3] |= 0x04 ;
				}

				break;
		case 3:
				RX_POWER_CH3 = GET_RSSI_CH0_CH3( 0 );
                offset_Buffer_RXP = (CALIB_MEMORY_MAP.RX3_OFFSET_MSB << 8 | CALIB_MEMORY_MAP.RX3_OFFSET_LSB );
                RXLOS_Assert_Buffer = (CALIB_MEMORY_MAP.Rx3_LOS_Assret_MSB << 8 | CALIB_MEMORY_MAP.Rx3_LOS_Assret_LSB  );
                RXLOS_DeAssert_Buffer = (CALIB_MEMORY_MAP.Rx3_LOS_DeAssret_MSB << 8 | CALIB_MEMORY_MAP.Rx3_LOS_DeAssret_LSB  );    
        
				RX_POWER_CH3 = CAL_Function( RX_POWER_CH3 , CALIB_MEMORY_MAP.RX3_SCALEM , CALIB_MEMORY_MAP.RX3_SCALEL , offset_Buffer_RXP );
				if(RX_POWER_CH3>0xF000)
					RX_POWER_CH3 = 0;
				// RX RSSI CH 3
				if( RX_POWER_CH3 < RXLOS_Assert_Buffer )
					SHOW_RSSI_CH3 = SHOW_RSSI_Disable;

				if( RX_POWER_CH3 >= RXLOS_DeAssert_Buffer )
					SHOW_RSSI_CH3 = SHOW_RSSI_Enable;

				if( SHOW_RSSI_CH3 == SHOW_RSSI_Enable )
				{
					SFF8636_A0[40] = RX_POWER_CH3 >> 8 ;
					SFF8636_A0[41] = RX_POWER_CH3 ;
				}
				else
				{
					SFF8636_A0[40] = 0;
					SFF8636_A0[41] = 1 ;

					SFF8636_A0[3] |= 0x08 ;
				}
				break;

		default:
			    break;
	}
}

void SFF8636_DDMI_StateMachine(uint8_t StateMachine )
{
	switch(StateMachine)
	{
		case ADC_SM:
					Temperature_Monitor(GET_GD_Temperature());
					VCC_Monitor( GET_ADC_Value_Data( P3V3_TX_Mon )*2 );
					break;
        
		case TXB_TXP_01_SM:
                    if(CALIB_MEMORY_MAP.LUT_EN)
                    {
                        IBIAS_LUT_CONTROL_TX1_TX4(0); // Bias & mod LUT Control
                        IBIAS_LUT_CONTROL_TX1_TX4(1); // Bias & mod LUT Control
                    }
					BIAS_TxPower_Monitor( 0 );
                    BIAS_TxPower_Monitor( 1 );
					break;

		case TXB_TXP_23_SM:
                    if(CALIB_MEMORY_MAP.LUT_EN)
                    {
                        IBIAS_LUT_CONTROL_TX1_TX4(2); // Bias & mod LUT Control
                        IBIAS_LUT_CONTROL_TX1_TX4(3); // Bias & mod LUT Control
                    }
					BIAS_TxPower_Monitor( 2 );
                    BIAS_TxPower_Monitor( 3 );
					break;

		case RXP_01_SM :
					RX_POWER_M_CH0_CH4(0);
                    RX_POWER_M_CH0_CH4(1);
        
					break;
		case RXP_23_SM :
					RX_POWER_M_CH0_CH4(2);
                    RX_POWER_M_CH0_CH4(3);
                    break;

		case OP_SM:
					if(Power_on_flag==1)
                        SFF8636_DDMI_AW();
					
					break;
		case Other_DDMI:
					
                    Get_module_Power_Monitor();
					break;

		default:
				break;
	}
}


