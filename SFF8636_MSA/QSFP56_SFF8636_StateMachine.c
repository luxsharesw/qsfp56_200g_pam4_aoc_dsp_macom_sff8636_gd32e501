#include "gd32e501.h"
#include "core_cm33.h"
#include "systick.h"
#include <stdint.h>
#include "MA38435_TX1_TX4.h"
#include "MA38434_RX1_RX4.h"
#include "Calibration_Struct.h"
#include "Calibration_Struct_2.h"
#include "Master_I2C1_PB3_PB4.h"
#include "BRCM_DSP_87540.h"
#include "SFF8636_MSA.h"
#include "MCU_GPIO_Customize_Define.h"
//--------------------------------------------------------//
//MODULE State
//--------------------------------------------------------//
#define MODULE_MGMT_INIT    0x00
#define MODULE_LOW_PWR      0x10
#define MODULE_PWR_UP       0x20
#define MODULE_READY        0x30
#define MODULE_PWR_DN       0x40
#define MODULE_FAULT        0x50
//--------------------------------------------------------//
//DATA PATH State
//--------------------------------------------------------//
#define DEACTIVATED_DATHPATH  1
#define INIT_DATHPATH         2
#define DATHPATH_DEINIT       3
#define ACTIVATED_DATHPATH    4

uint8_t Module_Status = 0 ;
uint8_t SoftWare_Init_flag = 0 ;
uint8_t DDMI_Update_C = 0 ;
uint8_t DSP_MODE_SET = 0x00;
uint8_t Power_on_flag = 0;
uint16_t DDMI_Count = 0;
uint16_t  DDMI_DataCount = 0 ;
uint8_t Bef_Module_Global_Control = 0x00 ;

//Control Flag
uint8_t TxDIS_Flag=0;
uint8_t Tx_output_Flag=0;

//PAM4 or NRZ Mode Select
uint8_t Signal_Status=0;

extern uint8_t Get_Power_C_Status();

void Get_Selected_Chip_Mode()
{
    uint8_t Data_Buffer;
    //Get DSP Chip Mode LSB Data
    Data_Buffer = BRCM_87540_DSP_LS_PHY0_MEMORY_MAP.DSP_CHIP_MODE>>8;

    //Mode 1 CHIP_MODE_4X53G_PAM4_4X53G_PAM4 DSP Default Mode
   	if(Data_Buffer==0x00)
    {
        DSP_MODE_SET=Mode1_4x53G_PAM4_4x53G_PAM4_1PORT;
        //PAM4 to PAM4 Mode
        Signal_Status=PAM4_to_PAM4;
    }
    //Mode 2 CHIP_MODE_2X53G_PAM4_2X53G_PAM4
    else if(Data_Buffer==0x01)
    {
        DSP_MODE_SET=Mode2_2x53G_PAM4_2x53G_PAM4_2PORT;
        //PAM4 to PAM4 Mode
        Signal_Status=PAM4_to_PAM4;
    }
    //Mode 3 CHIP_MODE_4X25G_NRZ_4X25G_NRZ
    else if(Data_Buffer==0x02)
    {
        DSP_MODE_SET=Mode3_4x25G_NRZ_4x25G_NRZ_1PORT;
        //NRZ to NRZ Mode
        Signal_Status=NRZ_to_NRZ;
    }
    //Mode 4 CHIP_MODE_1X53G_PAM4_1X53G_PAM4
    else if(Data_Buffer==0x03)
    {
        DSP_MODE_SET = Mode4_1x53G_PAM4_1x53G_PAM4_4PORT;
        //PAM4 to PAM4 Mode
        Signal_Status=PAM4_to_PAM4;
    }
    //Mode 5 CHIP_MODE_4X26G_NRZ_2X53G_PAM4
    else if(Data_Buffer==0x04)
    {
        DSP_MODE_SET=Mode5_4x26G_NRZ_2x53G_PAM4_1PORT;
        //NRZ to PAM4 Mode
        Signal_Status=NRZ_to_PAM4;
    }
    //Mode 6 CHIP_MODE_2X26G_NRZ_1X53G_PAM4
    else if(Data_Buffer==0x05)
    {   
        DSP_MODE_SET=Mode6_2x26G_NRZ_1x53G_PAM4_2PORT;
        //NRZ to PAM4 Mode
        Signal_Status=NRZ_to_PAM4;
    }
    //Mode 7 CHIP_MODE_2X25G_NRZ_1X50G_PAM4
    else if(Data_Buffer==0x06)
    {
        DSP_MODE_SET=Mode7_2x25G_NRZ_1x50G_PAM4_2PORT;
        //NRZ to PAM4 Mode
        Signal_Status=NRZ_to_PAM4;
    }
    //Mode 8 CHIP_MODE_4X25G_NRZ_2X50G_PAM4
    else if(Data_Buffer==0x07)
    {
        DSP_MODE_SET=Mode8_4X25G_NRZ_2X50G_PAM4_1PORT;
        //NRZ to PAM4 Mode
        Signal_Status=NRZ_to_PAM4;
    }
    //Mode 9 CHIP_MODE_1X10G_NRZ_1X10G_NRZ
    else if(Data_Buffer==0x08)
    {
        DSP_MODE_SET=Mode9_1x10G_NRZ_1x10G_NRZ_4PORT;
        //NRZ to NRZ Mode
        Signal_Status=NRZ_to_NRZ;
    }
    //Mode 10 CHIP_MODE_2X26G_NRZ_1X53G_PAM4
    else if(Data_Buffer==0x09)
    {
        DSP_MODE_SET=Mode10_2x26G_NRZ_1x53G_PAM4_2PORT;
        //NRZ to PAM4 Mode
        Signal_Status=NRZ_to_PAM4;
    }
    //Mode 11 CHIP_MODE_2X26G_NRZ_1X53G_PAM4
    else if(Data_Buffer==0x0A)
    {
        DSP_MODE_SET=Mode11_2x25G_NRZ_1x50G_PAM4_2PORT;
        //NRZ to PAM4 Mode
        Signal_Status=NRZ_to_PAM4;
    }
    //Mode 12 CHIP_MODE_4X26G_NRZ_4X26G_NRZ
    else if(Data_Buffer==0x0B)
    {
        DSP_MODE_SET=Mode12_4x26G_NRZ_4x26G_NRZ_1PORT;
        //NRZ to NRZ Mode
        Signal_Status=NRZ_to_NRZ;
    }
    //Mode 13 CHIP_MODE_1X25G_NRZ_1X25G_NRZ
    else if(Data_Buffer==0x0C)
    {
        DSP_MODE_SET=Mode13_1x25G_NRZ_1x25G_NRZ_4PORT;
        //NRZ to NRZ Mode
        Signal_Status=NRZ_to_NRZ;
    }
    //Mode 14 CHIP_MODE_2X26G_NRZ_2X26G_NRZ
    else if(Data_Buffer==0x0D)
    {
        DSP_MODE_SET=Mode14_2x26G_NRZ_2x26G_NRZ_2PORT;
        //NRZ to NRZ Mode
        Signal_Status=NRZ_to_NRZ;
    }
    //Mode 15 CHIP_MODE_2X25G_NRZ_2X25G_NRZ
    else if(Data_Buffer==0x0E)
    {
        DSP_MODE_SET=Mode15_2x25G_NRZ_2x25G_NRZ_2PORT;
        //NRZ to NRZ Mode
        Signal_Status=NRZ_to_NRZ;
    }
    //Mode 16 CHIP_MODE_2X25G_NRZ_1X53G_PAM4
    else if(Data_Buffer==0x0F)
    {
        DSP_MODE_SET=Mode16_2x25G_NRZ_1x53G_PAM4_2PORT;
        //NRZ to PAM4 Mode
        Signal_Status=NRZ_to_PAM4;
    }
    //Mode 17 CHIP_MODE_4X25G_NRZ_2X53G_PAM4
    else if(Data_Buffer==0x10)
    {
        DSP_MODE_SET=Mode17_4x25G_NRZ_2x53G_PAM4_1PORT;
        //NRZ to PAM4 Mode
        Signal_Status=NRZ_to_PAM4;
    }
    //Mode 18 CHIP_MODE_4X50G_PAM4_4X50G_PAM4
    else if(Data_Buffer==0x11)
    {
        DSP_MODE_SET=Mode18_4x50G_PAM4_4x50G_PAM4_1PORT;
        //PAM4 to PAM4 Mode
        Signal_Status=PAM4_to_PAM4;
    }
    //Mode 19 CHIP_MODE_2X25G_NRZ_1X53G_PAM4
    else if(Data_Buffer==0x12)
    {
        DSP_MODE_SET=Mode19_2x50G_NRZ_1x53G_PAM4_2PORT;
        //NRZ to PAM4 Mode
        Signal_Status=NRZ_to_PAM4;
    }
}

void Get_module_Power_Monitor()
{
    uint16_t GetADC_Buffer ;
    //--------------------------------------------------------------//
    // P1V8 Monitor
    //--------------------------------------------------------------//
    GetADC_Buffer = GET_ADC_Value_Data( P1V8_Mon );
    CALIB_MEMORY_1_MAP.TX_CDR_P1V8_ADC_MSB = GetADC_Buffer >> 8 ;
    CALIB_MEMORY_1_MAP.TX_CDR_P1V8_ADC_LSB = GetADC_Buffer ;
    //--------------------------------------------------------------//
    // TX P3V3 Monitor
    //--------------------------------------------------------------//  
    GetADC_Buffer = GET_ADC_Value_Data( P3V3_TX_Mon )*2 ;
    CALIB_MEMORY_1_MAP.TX_P3V3_ADC_MSB = GetADC_Buffer >> 8 ;
    CALIB_MEMORY_1_MAP.TX_P3V3_ADC_LSB = GetADC_Buffer ;
    //--------------------------------------------------------------//
    // RX P3V3 Monitor
    //--------------------------------------------------------------//  
    GetADC_Buffer = GET_ADC_Value_Data( P3V3_RX_Mon )*2 ;
    CALIB_MEMORY_1_MAP.RX_P3V3_ADC_MSB = GetADC_Buffer >> 8 ;
    CALIB_MEMORY_1_MAP.RX_P3V3_ADC_LSB = GetADC_Buffer ;    
    //--------------------------------------------------------------//
    // Temperature Monitor
    //--------------------------------------------------------------//   
    GetADC_Buffer = GET_GD_Temperature();   
	CALIB_MEMORY_1_MAP.TEMP_ADC_MSB = GetADC_Buffer >> 8 ;
    CALIB_MEMORY_1_MAP.TEMP_ADC_LSB = GetADC_Buffer;
    //--------------------------------------------------------------//
    // PowerCntrol Enable status
    //--------------------------------------------------------------//         
    CALIB_MEMORY_1_MAP.Power_C_Status  = Get_Power_C_Status();
}

//------------------------------------------------------//
// Tx Squelch Enable 0x83
//------------------------------------------------------//
void MSA_Tx_Squelch_Control_P03H_BA231()
{
    uint8_t Data_Buffer=0x00;
    
    Data_Buffer=SFF8636_PAGE3[103];
    if(Tx_output_Flag==0) //Tx Squelch Control
    {
        if(Bef_Tx_output!=SFF8636_PAGE3[103])
        {
            Tx_output_Flag=1;
            Bef_Tx_output=SFF8636_PAGE3[103];
        }
    }
    else
    {
        // Tx1
        if(Data_Buffer&0x01)
        DSP87540_SystemSide_TRX_Squelch_SET( 0 , RX_Side , Function_EN );
        else
        DSP87540_SystemSide_TRX_Squelch_SET( 0 , RX_Side , Function_DIS );
        // Tx2
        if(Data_Buffer&0x02)
        DSP87540_SystemSide_TRX_Squelch_SET( 1 , RX_Side , Function_EN );
        else
        DSP87540_SystemSide_TRX_Squelch_SET( 1 , RX_Side , Function_DIS );
        // Tx3
        if(Data_Buffer&0x04)
        DSP87540_SystemSide_TRX_Squelch_SET( 2 , RX_Side , Function_EN );
        else
        DSP87540_SystemSide_TRX_Squelch_SET( 2 , RX_Side , Function_DIS );
        // Tx4
        if(Data_Buffer&0x08)
        DSP87540_SystemSide_TRX_Squelch_SET( 3 , RX_Side , Function_EN );
        else
        DSP87540_SystemSide_TRX_Squelch_SET( 3 , RX_Side , Function_DIS );
        
        Tx_output_Flag=0;
    }
//	DSP87540_SystemSide_TRX_Squelch_SET( ALL_CHANNEL , RX_Side , Function_EN );
}

//------------------------------------------------------//
// TX_DIS_FUNCTION
//------------------------------------------------------//
void TX_DIS_FUNCTION(uint8_t TxDIS_Control )
{
    if(TxDIS_Flag==0) // Tx Power control
    {
        if(Bef_TxDIS_Power!=TxDIS_Control)
        {
            TxDIS_Flag=1;
            Bef_TxDIS_Power=TxDIS_Control;
        }
    }
    if(TxDIS_Flag==1)
    {
        //--------------------------------------------------//
        // Tx1 MATA38435_TX1_TX4_MEMORY_MAP.TX_DISABLE
        //--------------------------------------------------//
        if( TxDIS_Control & 0x01 )
        MATA38435_TX1_TX4_MEMORY_MAP.TX_DISABLE |= 0x01 ;
        else
        MATA38435_TX1_TX4_MEMORY_MAP.TX_DISABLE &= ~0x01 ;
        //--------------------------------------------------//
        // Tx2
        //--------------------------------------------------//
        if( TxDIS_Control & 0x02 )
        MATA38435_TX1_TX4_MEMORY_MAP.TX_DISABLE |= 0x02 ;
        else
        MATA38435_TX1_TX4_MEMORY_MAP.TX_DISABLE &= ~0x02 ;
        //--------------------------------------------------//
        // Tx3
        //--------------------------------------------------//
        if( TxDIS_Control & 0x04 )
        MATA38435_TX1_TX4_MEMORY_MAP.TX_DISABLE |= 0x04 ;
        else
        MATA38435_TX1_TX4_MEMORY_MAP.TX_DISABLE &= ~0x04 ;
        //--------------------------------------------------//
        // Tx4
        //--------------------------------------------------//
        if( TxDIS_Control & 0x08 )
        MATA38435_TX1_TX4_MEMORY_MAP.TX_DISABLE |= 0x08 ;
        else
        MATA38435_TX1_TX4_MEMORY_MAP.TX_DISABLE &= ~0x08 ;

        // Tx1-Tx4 I2C is Master IC1
        Master_I2C1_ByteWrite_PB34( MALD38435_SADR , 0x1D , 0x7F );
        Master_I2C1_ByteWrite_PB34( MALD38435_SADR , 0x41 , MATA38435_TX1_TX4_MEMORY_MAP.TX_DISABLE );
        
        TxDIS_Flag=0;
    }    
}
//------------------------------------------------------//
// Lower Page Control
//------------------------------------------------------//
void Module_Software_RESET_P0_B93()
{
	if( Bef_Module_Global_Control!=SFF8636_A0[2] )
	{
        if(SFF8636_A0[2]&0x80)
            __NVIC_SystemReset();
	}

	Bef_Module_Global_Control = SFF8636_A0[2];
}

//------------------------------------------------------//
// Modeule_MGMT_INIT
//------------------------------------------------------//
void Modeule_MGMT_INIT()
{
	// LDD Disable
	TX_DIS_FUNCTION(0xFF);
	//DSP to Low Power Mode
    LPMODE_DSP_High();
     
//	if( CALIB_MEMORY_MAP.QDD_SoftInitM_EN == 0x01 )
//	{
		//if(gpio_input_bit_get(GPIOF, LPMODE_G_GPIOF)==1)
		//	SoftWare_Init_flag = 1 ;
		// HW Init Mode
	//	else
		///	SoftWare_Init_flag = 0 ;
	//}
	//else
	//	SoftWare_Init_flag = 0;


	Get_Selected_Chip_Mode();
	SFF8636_A0[2] = Module_Status;
	SFF8636_A0[6] |= 0x04;
    
    Module_Status = MODULE_LOW_PWR ;
    IntL_G_Low();
}

void QSFP28_MSA_StateMachine()
{
	switch( Module_Status )
	{
		case MODULE_MGMT_INIT:
			 Modeule_MGMT_INIT();
			 break;

		case MODULE_LOW_PWR:
			 // Software Init Mode
			// if(SoftWare_Init_flag==1)
			// {
             if(SFF8636_A0[93]&0x05)
             {
                 Module_Status = MODULE_PWR_UP ;
                 IntL_G_Low();
                 SFF8636_A0[2] = MODULE_PWR_UP;
                 SFF8636_A0[6] |= 0x05;
             }
/*               else if(SFF8636_PAGE30[0]==0x0F)
                 {
                     Module_Status = MODULE_PWR_UP ;                   
					 IntL_G_Low();
					 SFF8636_A0[2] = MODULE_PWR_UP;
					 SFF8636_A0[6] |= 0x04;
                 }*/
			// }
			// else
			// {
			//	 Module_Status = MODULE_PWR_UP ;                
            //   IntL_G_Low();
			//	 SFF8636_A0[2] = MODULE_PWR_UP;
			//	 SFF8636_A0[6] |= 0x05;
			// }

			 break;

		case MODULE_PWR_UP:

             // DSP to High Power Mode
             LPMODE_DSP_Low();
             // Disable I2C
             //i2c_disable(I2C0);
             delay_1ms(200);     //Must put delay for stable to set dsp mode
             DSP87540_Init(DSP_MODE_SET);
             // Enable I2C
             //i2c_enable(I2C0); 

			 Module_Status = MODULE_READY ;
			 SFF8636_A0[2] = MODULE_READY ;
			 SFF8636_A0[6] |= 0x05;

			 TX_DIS_FUNCTION(0x00);
			 SFF8636_PAGE30[0] = 0x0F ;
			 SFF8636_PAGE30[79] = 0x44 ;
			 SFF8636_PAGE30[80] = 0x44 ;
        
             IntL_G_Low();
			 break;

		case MODULE_READY:
			 // DDMI Function update
        	 if( CALIB_MEMORY_MAP.DDMI_DISABLE == 1)
             {
                Temperature_Monitor(GET_GD_Temperature());
                VCC_Monitor( GET_ADC_Value_Data( P3V3_TX_Mon )*2 );
             }
             else
             {
                 if( DDMI_DataCount > 100 )
                 {
                     // Status Machine Loop
                     SFF8636_DDMI_StateMachine( DDMI_Update_C );
                     DDMI_Update_C++;
                     //Status Machine flag
                     if( DDMI_Update_C >= 7 )
                     {
                         //Module_Software_RESET_P0_B93();
                         DDMI_Update_C = 0;
                         if(Power_on_flag==0)
                        {
                            DDMI_Count ++;
                            if(DDMI_Count>10)
                                Power_on_flag = 1;
                        }
                     }
                     DDMI_DataCount = 0;
                 }
				 
             }
			 DDMI_DataCount++;
			 break;

		case MODULE_PWR_DN:
			 break;

		case MODULE_FAULT:
			 break;
	}

	if( Module_Status != MODULE_READY )
	{
        Temperature_Monitor(GET_GD_Temperature());
        VCC_Monitor( GET_ADC_Value_Data( P3V3_TX_Mon )*2 );
	}

	if( Module_Status == MODULE_READY )
	{
        TX_DIS_FUNCTION(SFF8636_A0[86]);        // Tx Power control
        MSA_Tx_Squelch_Control_P03H_BA231();    //Tx Squelch Control
        MSA_Rate_Select_Control();
        Module_Soft_reset_Funciton();
	}
}





