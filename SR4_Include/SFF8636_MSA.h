//----------------------------------------------//
// Data SRAM include
//----------------------------------------------//
//----------------------------------------------//
// DDMI_AW.c
//----------------------------------------------//
#define ADDR_LOS           3
#define ADDR_TXFAULT       4
#define ADDR_CDR_LOL       5
#define ADDR_TEMP          6
#define ADDR_VCC           7
#define ADDR_RXP12         9
#define ADDR_RXP34        10
#define ADDR_TXB12        11
#define ADDR_TXB34        12
#define ADDR_TXP12        13
#define ADDR_TXP34        14

#define LOS_INT       0x0001  //1<<0
#define FAULT_INT     0x0002  //1<<1
#define TEMP_INT      0x0004  //1<<2
#define VCC_INT       0x0008  //1<<3
#define RX12_INT      0x0010  //1<<4
#define RX34_INT      0x0020  //1<<5
#define TXP12_INT     0x0040  //1<<6
#define TXP34_INT     0x0080  //1<<7
#define TXB12_INT     0x0100  //1<<8
#define TXB34_INT     0x0200  //1<<9
#define CDR_LOL_INT   0x0400  //1<<10
#define All_CH 4
//----------------------------------------------------//
//Signal Status
//----------------------------------------------------//
#define PAM4_to_PAM4 0
#define NRZ_to_NRZ   1
#define NRZ_to_PAM4  2

extern uint8_t SFF8636_A0[256];
extern uint8_t SFF8636_PAGE0[128];
extern uint8_t SFF8636_PAGE1[128];
extern uint8_t SFF8636_PAGE2[128];
extern uint8_t SFF8636_PAGE3[128];
extern uint8_t SFF8636_PAGE30[128];
extern uint16_t CHECKSUM;

extern uint8_t CTEL_Table[16];
extern uint8_t EMPHASIS_Table[16];
extern uint8_t OP_AMP_Table[16];
extern uint8_t PW_LEVE1[4];
extern uint8_t DSP_MODE_SET;
//----------------------------------------------//
// GD32E501_Initialize.c
//----------------------------------------------//
void GD32E501_Power_on_Initial();
//----------------------------------------------//
// GD32E501_ADC_Control.c
//----------------------------------------------//
uint16_t GET_ADC_Value_Data(uint16_t ADC_CH);
uint16_t GET_GD_Temperature();
//----------------------------------------------//
// QSFP56_SR4_PowerOn_Sequencing.c
//----------------------------------------------//
void ModSelL_Function();
void PowerOn_Sequencing_Control();
void SET_Power_Control(uint8_t SET_Value);
uint8_t Get_Power_C_Status();
void MCU_READ_WRITE_DEIVCE_COMMAND_CONTROL();
void CheckSum_Calculate();
void RESET_L_Function();
void PowerOn_Reset_Check();
//----------------------------------------------//
// QSFP56_SR4_PowerOn_Table.c
//----------------------------------------------//
void PowerON_Table_Init();
uint8_t Get_CH_Data (uint8_t Data,uint8_t CH);
extern uint8_t Bef_TxDIS_Power;
extern void Get_NRZ_Default_System_Side();
extern uint8_t Bef_Tx_output;
//----------------------------------------------//
// GD32E501_Flash_RW.c
//----------------------------------------------//
void GDMCU_Flash_Erase(uint32_t FMC_ADR);
void GDMCU_FMC_READ_FUNCTION(uint32_t FMC_ADR,uint8_t *DataBuffer,uint16_t DATAL);
void GDMCU_FMC_BytesWRITE_FUNCTION(uint32_t FMC_ADR,uint8_t *Write_data,uint8_t DATAL);
uint8_t GDMCU_FMC_READ_DATA(uint32_t FMC_ADR);
uint16_t Swap_Bytes(uint16_t Data);
//----------------------------------------------//
// DDMI_AW.c
//----------------------------------------------//
void SFF8636_DDMI_StateMachine(unsigned char StateMachine);
void QSFP28_Temperature_AW();
void QSFP28_VCC_AW();
void QSFP28_RxPower_AW();
void RX_DC_POWER_Monitor(unsigned char Channel);
void SFF8636_DDMI_AW();
void Clear_Flag(unsigned char Current_AD);
//----------------------------------------------//
// QSFPDD_MSA_DDMI.c
//----------------------------------------------//
void Temperature_Monitor(int16_t tempsensor_value);
void VCC_Monitor( uint16_t VCC_Vaule);
extern int8_t Temperature_Index;
//----------------------------------------------------//
//EFM8LB_SMBUS_ISR.c
//----------------------------------------------------//
void SRMA_Flash_Function();
//----------------------------------------------//
// MSA_StateMachine.c
//----------------------------------------------//
void Get_module_Power_Monitor();
void QSFP28_MSA_StateMachine();
extern uint8_t Signal_Status;
//----------------------------------------------//
// QSFP56_SFF8636_Options.c
//----------------------------------------------//
void MSA_Rate_Select_Control();
void Module_Soft_reset_Funciton();

void Get_Selected_Chip_Mode();
