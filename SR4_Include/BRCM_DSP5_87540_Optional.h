/*
 * BRCM_DSP58281_Optional.h
 *
 *  Created on: 2019�~8��28��
 *      Author: Ivan_Lin
 */

#ifndef INC_BRCM_DSP87540_OPTIONAL_H_
#define INC_BRCM_DSP87540_OPTIONAL_H_

uint16_t DSP87540_Line_Side_LOL();
uint16_t DSP87540_System_Side_LOS();
uint16_t DSP87540_System_Side_LOL();
void DSP87540_System_Side_LOS_LOL(uint8_t *LOS,uint8_t *LOL);
void DSP87540_Line_Side_LOS_LOL(uint8_t *LOS,uint8_t *LOL);
#endif /* INC_BRCM_DSP58281_OPTIONAL_H_ */
