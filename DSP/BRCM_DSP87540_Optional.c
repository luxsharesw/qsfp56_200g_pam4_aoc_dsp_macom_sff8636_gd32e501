#include "gd32e501.h"
#include "core_cm33.h"
#include "systick.h"
#include "Master_I2C2_PC78.h"
#include "BRCM_DSP87540_RW.h"
#include "BRCM_DSP_87540.h"
#include "string.h"
#include "math.h"
#include "SFF8636_MSA.h"

// SNR Data
uint32_t slicer_threshold[3]={0};
uint32_t slicer_target[4]={0};
uint32_t slicer_mean_value[4]={0};
float slicer_sigma_value[4]={0};
double slicer_p_value[4]={0};
uint32_t slicer_p_location[4]={0};
double slicer_v_value[3]={0};
uint32_t slicer_v_location[3]={0};
uint16_t DSP_LineSide_SNR=0x0000;
uint16_t DSP_LineSide_LTP=0x0000;

//-----------------------------------------------------------------------------------------------------//
// DSP87540 SystemSide Digital/Remote Loopback function
// LoopBack_mode = 1  Digital Loopback
// LoopBack_mode = 0  remote Loopbcak
//-----------------------------------------------------------------------------------------------------//
void DSP87540_SystemSide_Loopback_SET(uint8_t LoopBack_mode,uint8_t Lane_CH,uint8_t Enable)
{
    if(Lane_CH==0)
    {
        COMMAND_4700_DATA[0]  = 0x00000002 ;
        DSP87540_CAPI_CW_CMD( 0x5201CA88 , 0x00000001 , 0x5201CA84 , 0x0000800A );
    }
    else if(Lane_CH==1)
    {
        COMMAND_4700_DATA[0]  = 0x00010002 ;
        DSP87540_CAPI_CW_CMD( 0x5201CA88 , 0x00000002 , 0x5201CA84 , 0x0000800A );
    }
    else if(Lane_CH==2)
    {
        COMMAND_4700_DATA[0]  = 0x00020002 ;
        DSP87540_CAPI_CW_CMD( 0x5201CA88 , 0x00000004 , 0x5201CA84 , 0x0000800A );
    }
    else if(Lane_CH==3)
    {
        COMMAND_4700_DATA[0]  = 0x00030002 ;
        DSP87540_CAPI_CW_CMD( 0x5201CA88 , 0x00000008 , 0x5201CA84 , 0x0000800A );
    }
     //Digital Loopback
    if(LoopBack_mode==1)
    {
        if(Enable)
            COMMAND_4700_DATA[1]  = 0xCCCC0000 | 0x0100 ;
        else
            COMMAND_4700_DATA[1]  = 0xCCCC0000 & ~0x0100;
    }
    //Remote Loopback
    else
    {
        if(Enable)
            COMMAND_4700_DATA[1]  = 0xCCCC0001 | 0x0100 ;
        else
            COMMAND_4700_DATA[1]  = 0xCCCC0001 & ~0x0100 ;
    }
           
    DSP87540_PAM4_COMMAND_ID_SET_CONFIG_INFO_CORE_IP_CW(0x5201CA84);
    DSP87540_CAPI_Command_response(0x5201CA8C,0x5201CA90);        
}

//-----------------------------------------------------------------------------------------------------//
// DSP87540 SystemSide TRX Squelch
// TRX_Side = 1  TX Squelch ( Moudle Rx ouput )
// TRX_Side = 0  RX Squelch
//-----------------------------------------------------------------------------------------------------//
void DSP87540_SystemSide_TRX_Squelch_SET(uint8_t Lane_CH ,uint8_t TRX_Side,uint8_t Enable)
{  
    if(Lane_CH==0)    
    {
        COMMAND_4700_DATA[0] = 0x00000008 ;
        DSP87540_CAPI_CW_CMD( 0x5201CA88 , 0x00000001 , 0x5201CA84 , 0x00008006 );
    }
    else if(Lane_CH==1)    
    {
        COMMAND_4700_DATA[0] = 0x00010008 ;
        DSP87540_CAPI_CW_CMD( 0x5201CA88 , 0x00000002 , 0x5201CA84 , 0x00008006 );
    }
    else if(Lane_CH==2)   
    {
        COMMAND_4700_DATA[0] = 0x00020008 ;        
        DSP87540_CAPI_CW_CMD( 0x5201CA88 , 0x00000004 , 0x5201CA84 , 0x00008006 );
    }
    else if(Lane_CH==3)   
    {
        COMMAND_4700_DATA[0] = 0x00030008 ;           
        DSP87540_CAPI_CW_CMD( 0x5201CA88 , 0x00000008 , 0x5201CA84 , 0x00008006 );
    }
    else if(Lane_CH==0x0F)
    {
        COMMAND_4700_DATA[0] = 0x00000008 ;           
        DSP87540_CAPI_CW_CMD( 0x5201CA88 , 0x0000000F , 0x5201CA84 , 0x00008006 );
    }        
     
    if(TRX_Side)
    {
        COMMAND_4700_DATA[1] = 0x00000200;
        if(Enable)
            COMMAND_4700_DATA[2] = 0x00000000 | 0x00000800 ;
        else
            COMMAND_4700_DATA[2] = 0x00000000 & ~0x00000800 ;
    }
    else
    {
        COMMAND_4700_DATA[1] = 0x00000400;
        if(Enable)
            COMMAND_4700_DATA[2] = 0x00000000 | 0x00001000 ;
        else
            COMMAND_4700_DATA[2] = 0x00000000 & ~0x00001000 ;
    }  
    
    DSP87540_PAM4_COMMAND_ID_SET_CONFIG_INFO_CORE_IP_CW(0x5201CA84);
    DSP87540_CAPI_Command_response(0x5201CA8C,0x5201CA90);     
}

//-----------------------------------------------------------------------------------------------------//
// DSP87540 Line Digital/Remote Loopback function
// LoopBack_mode = 1  Digital Loopback
// LoopBack_mode = 0  remote Loopbcak
//-----------------------------------------------------------------------------------------------------//
void DSP87540_LineSide_Loopback_SET(uint8_t LoopBack_mode,uint8_t Lane_CH,uint8_t Enable)
{
    if(Lane_CH==0)
    {
        COMMAND_4700_DATA[0]  = 0x00000002 ;
        DSP87540_CAPI_CW_CMD( 0x5201CA98 , 0x00000001 , 0x5201CA94 , 0x0000800A );
    }
    else if(Lane_CH==1)
    {
        COMMAND_4700_DATA[0]  = 0x00010002 ;
        DSP87540_CAPI_CW_CMD( 0x5201CA98 , 0x00000002 , 0x5201CA94 , 0x0000800A );
    }
    else if(Lane_CH==2)
    {
        COMMAND_4700_DATA[0]  = 0x00020002 ;
        DSP87540_CAPI_CW_CMD( 0x5201CA98 , 0x00000004 , 0x5201CA94 , 0x0000800A );
    }
    else if(Lane_CH==3)
    {
        COMMAND_4700_DATA[0]  = 0x00030002 ;
        DSP87540_CAPI_CW_CMD( 0x5201CA98 , 0x00000008 , 0x5201CA94 , 0x0000800A );
    }
    //Digital Loopback
    if(LoopBack_mode==1)
    {
        if(Enable)
            COMMAND_4700_DATA[1]  = 0xCCCC0000 | 0x0100 ;
        else
            COMMAND_4700_DATA[1]  = 0xCCCC0000 & ~0x0100;
    }
    //Remote Loopback
    else
    {
        if(Enable)
            COMMAND_4700_DATA[1]  = 0xCCCC0001 | 0x0100 ;
        else
            COMMAND_4700_DATA[1]  = 0xCCCC0001 & ~0x0100 ;
    }
           
    DSP87540_PAM4_COMMAND_ID_SET_CONFIG_INFO_CORE_IP_CW(0x5201CA94);
    DSP87540_CAPI_Command_response(0x5201CA9C,0x5201CAA0);        
}

//-----------------------------------------------------------------------------------------------------//
// DSP87540 LineSide TRX Squelch
// TRX_Side = 1  TX Squelch ( Moudle Rx ouput )
// TRX_Side = 0  RX Squelch
//-----------------------------------------------------------------------------------------------------//
void DSP87540_LineSide_TRX_Squelch_SET(uint8_t Lane_CH ,uint8_t TRX_Side,uint8_t Enable)
{  
    if(Lane_CH==0)    
    {
        COMMAND_4700_DATA[0] = 0x00000008 ;
        DSP87540_CAPI_CW_CMD( 0x5201CA98 , 0x00000001 , 0x5201CA94 , 0x00008006 );
    }
    else if(Lane_CH==1)    
    {
        COMMAND_4700_DATA[0] = 0x00010008 ;
        DSP87540_CAPI_CW_CMD( 0x5201CA98 , 0x00000002 , 0x5201CA94 , 0x00008006 );
    }
    else if(Lane_CH==2)   
    {
        COMMAND_4700_DATA[0] = 0x00020008 ;        
        DSP87540_CAPI_CW_CMD( 0x5201CA98 , 0x00000004 , 0x5201CA94 , 0x00008006 );
    }
    else if(Lane_CH==3)   
    {
        COMMAND_4700_DATA[0] = 0x00030008 ;           
        DSP87540_CAPI_CW_CMD( 0x5201CA98 , 0x00000008 , 0x5201CA94 , 0x00008006 );
    }
    else if(Lane_CH==All_CH)
    {
        COMMAND_4700_DATA[0] = 0x00040008 ;           
        DSP87540_CAPI_CW_CMD( 0x5201CA98 , 0x0000000F , 0x5201CA94 , 0x00008006 );
    }
    if(TRX_Side)
    {
        COMMAND_4700_DATA[1] = 0x00000200;
        if(Enable)
            COMMAND_4700_DATA[2] = 0x00000000 | 0x00000800 ;
        else
            COMMAND_4700_DATA[2] = 0x00000000 & ~0x00000800 ;
    }
    else
    {
        COMMAND_4700_DATA[1] = 0x00000400;
        if(Enable)
            COMMAND_4700_DATA[2] = 0x00000000 | 0x00001000 ;
        else
            COMMAND_4700_DATA[2] = 0x00000000 & ~0x00001000 ;
    }  
    
    DSP87540_PAM4_COMMAND_ID_SET_CONFIG_INFO_CORE_IP_CW(0x5201CA94);
    DSP87540_CAPI_Command_response(0x5201CA9C,0x5201CAA0);     
}
//-----------------------------------------------------------------------------------------------------//
// DSP87540 System Side PRBS Controls
// 
// 
//-----------------------------------------------------------------------------------------------------//
void DSP87540_SystemSide_PRBS_SET(uint8_t Pattern ,uint8_t Lane_CH ,uint8_t Enable)
{
    //Lane Select
    if(Lane_CH==0)
    {
        COMMAND_4700_DATA[0]  = 0x0000002C ;
        DSP87540_CAPI_CW_CMD( 0x5201CA88 , 0x00000001 , 0x5201CA84 , 0x0000800C );
    }
    else if(Lane_CH==1)
    {
        COMMAND_4700_DATA[0]  = 0x0001002C ;
        DSP87540_CAPI_CW_CMD( 0x5201CA88 , 0x00000002 , 0x5201CA84 , 0x0000800C );
    }
    else if(Lane_CH==2)
    {
        COMMAND_4700_DATA[0]  = 0x00020002C ;
        DSP87540_CAPI_CW_CMD( 0x5201CA88 , 0x00000004 , 0x5201CA84 , 0x0000800C );
    }
    else if(Lane_CH==3)
    {
        COMMAND_4700_DATA[0]  = 0x0003002C ;
        DSP87540_CAPI_CW_CMD( 0x5201CA88 , 0x00000008 , 0x5201CA84 , 0x0000800C );
    }
    
    //PRBS Generator
    if(Pattern!=SSPRQ_SystemSide)
        COMMAND_4700_DATA[1]  = 0x00000001;
    else
        COMMAND_4700_DATA[1]  = 0x00000004;
    
    //PRBS Enable
    if(Enable)
        COMMAND_4700_DATA[2]  = 0x00000001;
    else
        COMMAND_4700_DATA[2]  = 0x00000000;
    
    //PRBS Pattern
    if(Pattern!=SSPRQ_SystemSide)
        COMMAND_4700_DATA[3] = 0x00000000 | (Pattern<<8);
    //SSPRQ
    else
        COMMAND_4700_DATA[3] = 0x00000000;
    
    COMMAND_4700_DATA[4] = 0x00000000;
    COMMAND_4700_DATA[5] = 0x00000000;
    COMMAND_4700_DATA[6] = 0x00000000;
    COMMAND_4700_DATA[7] = 0x00000000;
    COMMAND_4700_DATA[8] = 0x00000000;
    COMMAND_4700_DATA[9] = 0x00000000;
    COMMAND_4700_DATA[10] = 0x00000000;
    COMMAND_4700_DATA[11] = 0x00000000;
    
    DSP87540_PAM4_COMMAND_ID_SET_CONFIG_INFO_CORE_IP_CW(0x5201CA84);
    DSP87540_CAPI_Command_response(0x5201CA8C,0x5201CA90);   
}

//-----------------------------------------------------------------------------------------------------//
// DSP87540 Line Side PRBS Controls
// 
// 
//-----------------------------------------------------------------------------------------------------//
void DSP87540_LineSide_PRBS_SET(uint8_t Pattern ,uint8_t Lane_CH ,uint8_t Enable)
{
    //Lane Select
    if(Lane_CH==0)
    {
        COMMAND_4700_DATA[0]  = 0x0000002C ;
        DSP87540_CAPI_CW_CMD( 0x5201CA98 , 0x00000001 , 0x5201CA94 , 0x0000800C );
    }
    else if(Lane_CH==1)
    {
        COMMAND_4700_DATA[0]  = 0x0001002C ;
        DSP87540_CAPI_CW_CMD( 0x5201CA98 , 0x00000002 , 0x5201CA94 , 0x0000800C );
    }
    else if(Lane_CH==2)
    {
        COMMAND_4700_DATA[0]  = 0x00020002C ;
        DSP87540_CAPI_CW_CMD( 0x5201CA98 , 0x00000004 , 0x5201CA94 , 0x0000800C );
    }
    else if(Lane_CH==3)
    {
        COMMAND_4700_DATA[0]  = 0x0003002C ;
        DSP87540_CAPI_CW_CMD( 0x5201CA98 , 0x00000008 , 0x5201CA94 , 0x0000800C );
    }
    
    //PRBS Generator
    if(Pattern!=SSPRQ_LineSide)
        COMMAND_4700_DATA[1]  = 0x00000001;
    //SSPRQ Generator
    else
        COMMAND_4700_DATA[1]  = 0x00000004;
    
    //PRBS Enable
    if(Enable)
        COMMAND_4700_DATA[2]  = 0x00000001;
    else
        COMMAND_4700_DATA[2]  = 0x00000000;
    
    //PRBS Pattern
    if(Pattern!=SSPRQ_LineSide)
        COMMAND_4700_DATA[3] = 0x00000000 | Pattern;
    //SSPRQ
    else
        COMMAND_4700_DATA[3] = 0x00000007;
    
    COMMAND_4700_DATA[4] = 0x00000000;
    COMMAND_4700_DATA[5] = 0x00000000;
    COMMAND_4700_DATA[6] = 0x00000000;
    COMMAND_4700_DATA[7] = 0x00000000;
    COMMAND_4700_DATA[8] = 0x00000000;
    COMMAND_4700_DATA[9] = 0x00000000;
    COMMAND_4700_DATA[10] = 0x00000000;
    COMMAND_4700_DATA[11] = 0x00000000;
    
    DSP87540_PAM4_COMMAND_ID_SET_CONFIG_INFO_CORE_IP_CW(0x5201CA94);
    DSP87540_CAPI_Command_response(0x5201CA9C,0x5201CAA0);   
}

uint16_t DSP87540_System_Side_CDR_Lock()
{
    // System Side Input
    uint8_t CDR_LOCK_STATUS = 0x0000;
    
    CDR_LOCK_STATUS = BRCM_Control_READ_Data(0x5201CA38);
    
    return CDR_LOCK_STATUS;
}


uint16_t DSP87540_Line_Side_CDR_Lock()
{
    uint8_t CDR_LOCK_STATUS = 0x0000;
    // Line Side Input
    CDR_LOCK_STATUS = BRCM_Control_READ_Data(0x5201CA34);
    
    return CDR_LOCK_STATUS;
}

void DSP87540_System_Side_LOS_LOL(uint8_t *LOS,uint8_t *LOL)
{
    uint16_t LOL_LOS_STATUS,CDR_LOL_STAUTS;
    uint32_t Data_Buffer;
    // Read LOL LOS Status
    LOL_LOS_STATUS=BRCM_Control_READ_Data(0x5201CA68);
    // LOS Bit0-7
    *LOS=LOL_LOS_STATUS&0x000F;
    // LOL Bit8-15
    *LOL=(LOL_LOS_STATUS>>8)&0x000F;
     /* Read the CDR LOL status */
    CDR_LOL_STAUTS=BRCM_Control_READ_Data(0x5201CA38);
    CDR_LOL_STAUTS=CDR_LOL_STAUTS<<8;
    Data_Buffer=CDR_LOL_STAUTS&(LOL_LOS_STATUS&0x0F00);
    if(Data_Buffer>0)
    {
        BRCM_Control_WRITE_Data(0x5201CA70,Data_Buffer,BRCM_Default_Length);
        BRCM_Control_READ_Data (0x5201CA68);
        BRCM_Control_READ_Data (0x5201CA38);
        BRCM_Control_WRITE_Data(0x5201CA70,0x00000000,BRCM_Default_Length);
    }
}

void DSP87540_Line_Side_LOS_LOL(uint8_t *LOS,uint8_t *LOL)
{
    uint16_t LOL_LOS_STATUS,CDR_LOL_STAUTS;
    uint32_t Data_Buffer;
    // Read LOL LOS Status
    LOL_LOS_STATUS=BRCM_Control_READ_Data(0x5201CA64);
    // LOS Bit0-7
    *LOS=LOL_LOS_STATUS&0x000F;
    // LOL Bit8-15
    *LOL=(LOL_LOS_STATUS>>8)&0x000F;
     /* Read the CDR LOL status */
    CDR_LOL_STAUTS=BRCM_Control_READ_Data(0x5201CA34);
    CDR_LOL_STAUTS=CDR_LOL_STAUTS<<8;
    Data_Buffer=CDR_LOL_STAUTS&(LOL_LOS_STATUS&0x0F00);
    if(Data_Buffer>0)
    {
        BRCM_Control_WRITE_Data(0x5201CA6C,Data_Buffer,BRCM_Default_Length);
        BRCM_Control_READ_Data (0x5201CA64);
        BRCM_Control_READ_Data (0x5201CA34);
        BRCM_Control_WRITE_Data(0x5201CA6C,0x00000000,BRCM_Default_Length);
    }
}

void DSP_Vcc_Config_0V75(uint16_t Voltage)
{   
    uint32_t Data_Buffer=0x50B20000;
    if(Voltage<0x5000)
    {
        Voltage=0x5000;
    }
    
    // Do a read modify write to regC */
    BRCM_Control_WRITE_Data(0x4000B000,0x00000286,BRCM_Default_Length);
    BRCM_Control_WRITE_Data(0x4000B004,0x60B20000,BRCM_Default_Length);
    BRCM_Control_READ_Data(0x4000B000);
    BRCM_Control_READ_Data(0x4000B000);
    BRCM_Control_READ_Data(0x4000B004);
    
    Data_Buffer=Data_Buffer|Voltage;
    
    BRCM_Control_WRITE_Data(0x4000B000,0x00000286,BRCM_Default_Length);
    BRCM_Control_WRITE_Data(0x4000B004,Data_Buffer,BRCM_Default_Length);
    BRCM_Control_READ_Data(0x4000B000);
    BRCM_Control_READ_Data(0x4000B000);
    // Toggle by writing 1 to reg0[1] and then 0 to complete the transaction */
    BRCM_Control_WRITE_Data(0x4000B000,0x00000286,BRCM_Default_Length);
    BRCM_Control_WRITE_Data(0x4000B004,0x60820000,BRCM_Default_Length);
    BRCM_Control_READ_Data(0x4000B000);
    BRCM_Control_READ_Data(0x4000B000);
    Data_Buffer=(BRCM_Control_READ_Data(0x4000B004)|0x0002);
    
    BRCM_Control_WRITE_Data(0x4000B000,0x00000286,BRCM_Default_Length);
    BRCM_Control_WRITE_Data(0x4000B004,0x50820000|Data_Buffer,BRCM_Default_Length);
    BRCM_Control_READ_Data(0x4000B000);
    BRCM_Control_READ_Data(0x4000B000);
    
    BRCM_Control_WRITE_Data(0x4000B000,0x00000286,BRCM_Default_Length);
    BRCM_Control_WRITE_Data(0x4000B004,0x60820000,BRCM_Default_Length);
    BRCM_Control_READ_Data(0x4000B000);
    BRCM_Control_READ_Data(0x4000B000);
    Data_Buffer=(BRCM_Control_READ_Data(0x4000B004)&0xFFFD);

    BRCM_Control_WRITE_Data(0x4000B000,0x00000286,BRCM_Default_Length);
    BRCM_Control_WRITE_Data(0x4000B004,0x50820000|Data_Buffer,BRCM_Default_Length);
    BRCM_Control_READ_Data(0x4000B000);
    BRCM_Control_READ_Data(0x4000B000);
}

void DSP_Vcc_Config_0V90(uint16_t Voltage)
{   
    uint32_t Data_Buffer=0x51320000;
    if(Voltage<0x8000)
    {
        Voltage=0x8000;
    }
    // Do a read modify write to regC */
    BRCM_Control_WRITE_Data(0x4000B000,0x00000286,BRCM_Default_Length);
    BRCM_Control_WRITE_Data(0x4000B004,0x61320000,BRCM_Default_Length);
    BRCM_Control_READ_Data(0x4000B000);
    BRCM_Control_READ_Data(0x4000B000);
    BRCM_Control_READ_Data(0x4000B004);
    
    Data_Buffer=Data_Buffer|Voltage;
    
    BRCM_Control_WRITE_Data(0x4000B000,0x00000286,BRCM_Default_Length);
    BRCM_Control_WRITE_Data(0x4000B004,Data_Buffer,BRCM_Default_Length);
    BRCM_Control_READ_Data(0x4000B000);
    BRCM_Control_READ_Data(0x4000B000);
    // Toggle by writing 1 to reg0[1] and then 0 to complete the transaction */
    BRCM_Control_WRITE_Data(0x4000B000,0x00000286,BRCM_Default_Length);
    BRCM_Control_WRITE_Data(0x4000B004,0x61020000,BRCM_Default_Length);
    BRCM_Control_READ_Data(0x4000B000);
    BRCM_Control_READ_Data(0x4000B000);
    Data_Buffer=(BRCM_Control_READ_Data(0x4000B004)|0x0002);
    
    BRCM_Control_WRITE_Data(0x4000B000,0x00000286,BRCM_Default_Length);
    BRCM_Control_WRITE_Data(0x4000B004,0x51020000|Data_Buffer,BRCM_Default_Length);
    BRCM_Control_READ_Data(0x4000B000);
    BRCM_Control_READ_Data(0x4000B000);
    
    BRCM_Control_WRITE_Data(0x4000B000,0x00000286,BRCM_Default_Length);
    BRCM_Control_WRITE_Data(0x4000B004,0x61020000,BRCM_Default_Length);
    BRCM_Control_READ_Data(0x4000B000);
    BRCM_Control_READ_Data(0x4000B000);
    Data_Buffer=(BRCM_Control_READ_Data(0x4000B004)&0xFFFD);
    
    BRCM_Control_WRITE_Data(0x4000B000,0x00000286,BRCM_Default_Length);
    BRCM_Control_WRITE_Data(0x4000B004,0x51020000|Data_Buffer,BRCM_Default_Length);
    BRCM_Control_READ_Data(0x4000B000);
    BRCM_Control_READ_Data(0x4000B000);
}

uint16_t DEC_EQUAL_TO_HEX(uint16_t Data_Value)
{
    uint16_t Data_Buffer = 0;
    uint16_t Hex_Value = 0;
    // Data_Value Ex:1745 dec will convert to 1745 hex
    // Get Bit12-Bit15 Hex Data
    Data_Buffer = Data_Value/1000 ;
    Hex_Value |= 0x1000*Data_Buffer; 
    
    // Get Bit8-Bit11 Hex Data
    Data_Buffer = 0 ;
    // Remove Thousand value
    Data_Buffer = Data_Value%1000;
    Data_Buffer = Data_Buffer/100;
    Hex_Value |= 0x100*Data_Buffer; 
    
    //Get Bit4-Bit7 Hex Data
    Data_Buffer = 0 ;
    // Remove Hundred value
    Data_Buffer = Data_Value%100;
    Data_Buffer = Data_Buffer/10;
    Hex_Value |= 0x10*Data_Buffer; 
    
    //Get Bit0-Bit3 Hex Data
    Data_Buffer = 0 ;
    // Remove Ten value
    Data_Buffer = Data_Value%10;
    
    Hex_Value|=Data_Buffer;
    
    return Hex_Value;
}

uint16_t Calulate_SNR_DATA()
{
    uint8_t For_count=2,Iconnt=0;
    float SNR_Buffer=0,snr_lvl[3]={0};
    uint16_t SNR_DATA=0x0000;
    
    for(Iconnt=0;Iconnt<=For_count;Iconnt++)
    {
        SNR_Buffer=(float)(slicer_mean_value[Iconnt+1]-slicer_mean_value[Iconnt]);
        snr_lvl[Iconnt]=SNR_Buffer/(slicer_sigma_value[Iconnt]+slicer_sigma_value[Iconnt+1]);
        // snr value= 10*log10*256
        snr_lvl[Iconnt]=(log10(snr_lvl[Iconnt]))*2560;
    }
    // Get min data
    Iconnt=0;
    SNR_DATA=snr_lvl[0];
    for(Iconnt=0;Iconnt<For_count;Iconnt++)
    {
        if(SNR_DATA>snr_lvl[Iconnt+1])
            SNR_DATA=snr_lvl[Iconnt+1];
    }
    SNR_DATA=DEC_EQUAL_TO_HEX(SNR_DATA);
    return SNR_DATA;
}

uint16_t Calulate_LTP_DATA()
{
    uint8_t For_count=2,Iconnt=0;
    double LTP_Buffer=0,ltp_lvl[3]={65535};
    uint16_t LTP_DATA=0x0000;
    
    for(Iconnt=0;Iconnt<=For_count;Iconnt++)
    {
        LTP_Buffer=(slicer_p_value[Iconnt+1]+slicer_p_value[Iconnt]);
        LTP_Buffer=LTP_Buffer/2;
        ltp_lvl[Iconnt]=LTP_Buffer/slicer_v_value[Iconnt];
        // ltp value= 10*log10*256
        ltp_lvl[Iconnt]=(log10(ltp_lvl[Iconnt]))*2560;
    }
    // Get min data
    Iconnt=0;
    LTP_DATA=ltp_lvl[0];
    for(Iconnt=0;Iconnt<For_count;Iconnt++)
    {
        if(LTP_DATA>ltp_lvl[Iconnt+1])
            LTP_DATA=ltp_lvl[Iconnt+1];
    }
    LTP_DATA=DEC_EQUAL_TO_HEX(LTP_DATA);
    return LTP_DATA;
}
//-----------------------------------------------------------------------------------------------------//
// DSP87540 Line Side SNR LTP
// 
//-----------------------------------------------------------------------------------------------------//
void DSP87540_LineSide_SNR_LTP_GET(uint8_t Lane_CH)
{
    uint16_t SNR_DATA=0x0000;
    uint32_t Write_Buffer=0x00047004,Data_Buffer=0x00000000;
    uint16_t For_count=22,Iconnt=0;
    
    BRCM_Control_READ_Data(0x5201CA94);
    BRCM_Control_WRITE_Data(0x00047000,0x0000005C,BRCM_Default_Length);
    // Write 0x00047004 to 0x0004705C
    for(Iconnt=0;Iconnt<=For_count;Iconnt++)
    {
        BRCM_Control_WRITE_Data(Write_Buffer,0xCCCCCCCC,BRCM_Default_Length);
        Write_Buffer+=0x00000004;
    }
    // Write Lane
    BRCM_Control_WRITE_Data(0x5201CA98,0x00000001<<Lane_CH,BRCM_Default_Length);
    // Write Command
    BRCM_Control_WRITE_Data(0x5201CA94,0x00008017,BRCM_Default_Length);
    // Read for until ready
    Iconnt=0;
    For_count=1500;
    for(Iconnt=0;Iconnt<=For_count;Iconnt++)
    {
        Data_Buffer=BRCM_Control_READ_Data(0x5201CA9C);
        if(Data_Buffer==0x00008000)
            break;
        else
            delay_1ms(1);
    }
    // Read Select Lane
    BRCM_Control_READ_Data(0x5201CAA0);
    // Read 0x00047400
    BRCM_Control_READ_Data(0x00047400);
    //-----------------------------------------------------------//
    // Read slicer_threshold[0] and slicer_threshold[1]
    //-----------------------------------------------------------//
    Data_Buffer=BRCM_Control_READ_Data(0x00047404);
    slicer_threshold[0]=Data_Buffer&0x0000FFFF;
    slicer_threshold[1]=(Data_Buffer>>16)&0x0000FFFF;
    //-----------------------------------------------------------//
    // Read slicer_threshold[2] and slicer_target[0]
    //-----------------------------------------------------------//
    Data_Buffer=BRCM_Control_READ_Data(0x00047408);
    slicer_threshold[2]=Data_Buffer&0x0000FFFF;
    slicer_target[0]=(Data_Buffer>>16)&0x0000FFFF;
    //-----------------------------------------------------------//
    // Read slicer_target[1] and slicer_target[3]
    //-----------------------------------------------------------//
    Data_Buffer=BRCM_Control_READ_Data(0x0004740C);
    slicer_target[1]=Data_Buffer&0x0000FFFF;
    slicer_target[2]=(Data_Buffer>>16)&0x0000FFFF;
    slicer_target[3]=BRCM_Control_READ_Data(0x00047410)&0x0000FFFF;
    //-----------------------------------------------------------//
    // Read slicer_mean_value[0] - slicer_mean_value[3]
    //-----------------------------------------------------------//
    slicer_mean_value[0]=BRCM_Control_READ_Data(0x00047414);
    slicer_mean_value[1]=BRCM_Control_READ_Data(0x00047418);
    slicer_mean_value[2]=BRCM_Control_READ_Data(0x0004741C);
    slicer_mean_value[3]=BRCM_Control_READ_Data(0x00047420);
    //-----------------------------------------------------------//
    // Read slicer_sigma_value[0] - slicer_sigma_value[3]
    //-----------------------------------------------------------//
    slicer_sigma_value[0]=(float)sqrt(BRCM_Control_READ_Data(0x00047424));
    slicer_sigma_value[1]=(float)sqrt(BRCM_Control_READ_Data(0x00047428));
    slicer_sigma_value[2]=(float)sqrt(BRCM_Control_READ_Data(0x0004742C));
    slicer_sigma_value[3]=(float)sqrt(BRCM_Control_READ_Data(0x00047430));
    //-----------------------------------------------------------//
    // Read slicer_p_value[0] - slicer_p_value[3]
    //-----------------------------------------------------------//
    slicer_p_value[0]=BRCM_Control_READ_Data(0x00047434);
    slicer_p_value[1]=BRCM_Control_READ_Data(0x00047438);
    slicer_p_value[2]=BRCM_Control_READ_Data(0x0004743C);
    slicer_p_value[3]=BRCM_Control_READ_Data(0x00047440);
    //-----------------------------------------------------------//
    // Read slicer_p_location[0] - slicer_p_location[1]
    //-----------------------------------------------------------//
    Data_Buffer=BRCM_Control_READ_Data(0x00047444);
    slicer_p_location[0]=Data_Buffer&0x0000FFFF;
    slicer_p_location[1]=(Data_Buffer>>16)&0x0000FFFF;
    //-----------------------------------------------------------//
    // Read slicer_p_location[2] - slicer_p_location[3]
    //-----------------------------------------------------------//
    Data_Buffer=BRCM_Control_READ_Data(0x00047448);
    slicer_p_location[2]=Data_Buffer&0x0000FFFF;
    slicer_p_location[3]=(Data_Buffer>>16)&0x0000FFFF;
    //-----------------------------------------------------------//
    // Read slicer_v_value[0] - slicer_v_value[2]
    //-----------------------------------------------------------//
    slicer_v_value[0]=BRCM_Control_READ_Data(0x0004744C);
    slicer_v_value[1]=BRCM_Control_READ_Data(0x00047450);
    slicer_v_value[2]=BRCM_Control_READ_Data(0x00047454);
    //-----------------------------------------------------------//
    // Read slicer_v_location[0] - slicer_v_location[2]
    //-----------------------------------------------------------//
    Data_Buffer=BRCM_Control_READ_Data(0x00047458);
    slicer_v_location[0]=Data_Buffer&0x0000FFFF;
    slicer_v_location[1]=(Data_Buffer>>16)&0x0000FFFF;
    slicer_v_location[2]=BRCM_Control_READ_Data(0x0004745C)&0x0000FFFF;
    // Clear Register data
    BRCM_Control_WRITE_Data( 0x5201CA84 , 0x00000000 , BRCM_Default_Length );
    BRCM_Control_WRITE_Data( 0x5201CA94 , 0x00000000 , BRCM_Default_Length );
    BRCM_Control_WRITE_Data( 0x5201CA74 , 0x00000000 , BRCM_Default_Length );
    BRCM_Control_WRITE_Data( 0x5201CA9C , 0x00000000 , BRCM_Default_Length );
    BRCM_Control_WRITE_Data( 0x5201CAA0 , 0x00000000 , BRCM_Default_Length );
    
    DSP_LineSide_SNR=Calulate_SNR_DATA();
    DSP_LineSide_LTP=Calulate_LTP_DATA();
}