
#include "gd32e501.h"
#include "core_cm33.h"
#include "systick.h"
#include "Master_I2C2_PC78.h"
#include "BRCM_DSP87540_RW.h"
#include "string.h"

struct BRCM_D87540_DSP_MEMORY BRCM_D87540_DSP_MEMORY_MAP;
uint8_t BRCM_DSP_I2C_SAD = 0xA0 ;   // BSC Slave Address


void BRCM_87540_Default_Table() 
{
	memset( &BRCM_D87540_DSP_MEMORY_MAP , 0x00 , 128 ) ;
}

void BRCM_WRITE_Data( uint8_t *BRCM_ADDR , uint8_t *DataBuffer , uint8_t Data_Length )
{
	uint8_t ADDR_Byte3,ADDR_Byte2,ADDR_Byte1,ADDR_Byte0;
	uint8_t LEN_LSB,LEN_MSB ;
	uint8_t IIcount ;
    // GD MCU MSB & LSB Swap
	ADDR_Byte3 = *BRCM_ADDR++ ; 
	ADDR_Byte2 = *BRCM_ADDR++ ; 
	ADDR_Byte1 = *BRCM_ADDR++ ; 
	ADDR_Byte0 = *BRCM_ADDR++ ; 
    
    LEN_LSB = Data_Length ;
    LEN_MSB = 0x00 ;
	//LEN_LSB = ( Data_Length & 0xFF ) ;
	//LEN_MSB = ( ( Data_Length >> 8 ) & 0xFF ) ;
	// Step1 : Write 0xFF to QSFP PAGE SELECT Control register
	Master_I2C2_ByteWrite_PC78( BRCM_DSP_I2C_SAD , BRCM_PAGE_SELECT , 0xFF );
	// Step2 : Write INDADDR0 - INDADDR3
	Master_I2C2_ByteWrite_PC78( BRCM_DSP_I2C_SAD , BRCM_IND_ADDR0 , ADDR_Byte0 );
	Master_I2C2_ByteWrite_PC78( BRCM_DSP_I2C_SAD , BRCM_IND_ADDR1 , ADDR_Byte1 );
	Master_I2C2_ByteWrite_PC78( BRCM_DSP_I2C_SAD , BRCM_IND_ADDR2 , ADDR_Byte2 );
	Master_I2C2_ByteWrite_PC78( BRCM_DSP_I2C_SAD , BRCM_IND_ADDR3 , ADDR_Byte3 );
	// Step3 : Write Data Length
	Master_I2C2_ByteWrite_PC78( BRCM_DSP_I2C_SAD , BRCM_IND_LEN0 , LEN_LSB );
	Master_I2C2_ByteWrite_PC78( BRCM_DSP_I2C_SAD , BRCM_IND_LEN1 , LEN_MSB );
	// Step4 : Write Data to WFIFO 0x03 write command
	Master_I2C2_ByteWrite_PC78( BRCM_DSP_I2C_SAD , BRCM_IND_CTRL , 0x03 );
    
	for(IIcount=0;IIcount<LEN_LSB;IIcount++)
        Master_I2C2_ByteWrite_PC78( BRCM_DSP_I2C_SAD , BRCM_WFIFO , *DataBuffer++ );
/*    Master_I2C2_ByteWrite_PC78( BRCM_DSP_I2C_SAD , BRCM_WFIFO , Data_Buffer0 );
    Master_I2C2_ByteWrite_PC78( BRCM_DSP_I2C_SAD , BRCM_WFIFO , Data_Buffer1 );
    Master_I2C2_ByteWrite_PC78( BRCM_DSP_I2C_SAD , BRCM_WFIFO , Data_Buffer2 );
    Master_I2C2_ByteWrite_PC78( BRCM_DSP_I2C_SAD , BRCM_WFIFO , Data_Buffer3 );*/
}
 
void BRCM_READ_Data( uint8_t *BRCM_ADDR , uint8_t *DataBuffer , uint8_t Data_Length )
{
	uint8_t ADDR_Byte3,ADDR_Byte2,ADDR_Byte1,ADDR_Byte0;
	uint8_t LEN_LSB,LEN_MSB ;
	uint8_t IIcount = 0 , Count = 0 ;
    
    // GD MCU MSB & LSB Swap
	ADDR_Byte3 = *BRCM_ADDR++ ; 
	ADDR_Byte2 = *BRCM_ADDR++ ; 
	ADDR_Byte1 = *BRCM_ADDR++ ; 
	ADDR_Byte0 = *BRCM_ADDR++ ; 

    LEN_LSB = Data_Length ;
    LEN_MSB = 0x00 ;
	//LEN_LSB = ( Data_Length & 0xFF ) ;
	//LEN_MSB = ( ( Data_Length >> 8 ) & 0xFF ) ;
	// Step1 : Write 0xFF to QSFP PAGE SELECT Control register
	Master_I2C2_ByteWrite_PC78( BRCM_DSP_I2C_SAD , BRCM_PAGE_SELECT , 0xFF );
	// Step2 : Write INDADDR0 - INDADDR3
	Master_I2C2_ByteWrite_PC78( BRCM_DSP_I2C_SAD , BRCM_IND_ADDR0 , ADDR_Byte0 );
	Master_I2C2_ByteWrite_PC78( BRCM_DSP_I2C_SAD , BRCM_IND_ADDR1 , ADDR_Byte1 );
	Master_I2C2_ByteWrite_PC78( BRCM_DSP_I2C_SAD , BRCM_IND_ADDR2 , ADDR_Byte2 );
	Master_I2C2_ByteWrite_PC78( BRCM_DSP_I2C_SAD , BRCM_IND_ADDR3 , ADDR_Byte3 );
	// Step3 : Write Data Length
	Master_I2C2_ByteWrite_PC78( BRCM_DSP_I2C_SAD , BRCM_IND_LEN0 , LEN_LSB );
	Master_I2C2_ByteWrite_PC78( BRCM_DSP_I2C_SAD , BRCM_IND_LEN1 , LEN_MSB );
	// Step4 : Write Data to WFIFO 0x01 READ command
	Master_I2C2_ByteWrite_PC78( BRCM_DSP_I2C_SAD , BRCM_IND_CTRL , 0x01 );
        
	for(IIcount=0;IIcount<LEN_LSB;IIcount++)
       *DataBuffer++ = Master_I2C2_ByteREAD_PC78( BRCM_DSP_I2C_SAD , BRCM_RFIFO );      
}

void TEST_DSP_Command_Direct_Control()
{
    uint32_t Read_temp_buffer ;
	if( BRCM_D87540_DSP_MEMORY_MAP.RW_EN == 0x01 )
	{
		if( BRCM_D87540_DSP_MEMORY_MAP.Direct_Control == 0x00 )
            BRCM_WRITE_Data( &BRCM_D87540_DSP_MEMORY_MAP.BRCM_REG_ADDR_0 , &BRCM_D87540_DSP_MEMORY_MAP.WRITE_BUFFER_0 , BRCM_D87540_DSP_MEMORY_MAP.LENGITH_LSB );
		else if( BRCM_D87540_DSP_MEMORY_MAP.Direct_Control == 0x01 )
            BRCM_READ_Data( &BRCM_D87540_DSP_MEMORY_MAP.BRCM_REG_ADDR_0 , &BRCM_D87540_DSP_MEMORY_MAP.READ_BUFFER_0 , BRCM_D87540_DSP_MEMORY_MAP.LENGITH_LSB );

		BRCM_D87540_DSP_MEMORY_MAP.RW_EN = 0x00 ;
	}
}




