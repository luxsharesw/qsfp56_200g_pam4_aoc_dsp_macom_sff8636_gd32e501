#include "gd32e501.h"
#include "core_cm33.h"
#include "systick.h"
#include "Master_I2C2_PC78.h"
#include "Master_I2C1_PB3_PB4.h"
#include "BRCM_DSP_87540.h"
#include "BRCM_DSP87540_RW.h"
#include "MA38435_TX1_TX4.h"
#include "MA38434_RX1_RX4.h"
#include "Calibration_Struct.h"
#include "Calibration_Struct_2.h"
#include "SFF8636_MSA.h"

int main(void)
{
    // GD MCU Initialize
    GD32E501_Power_on_Initial();
    // Power on HW RESET PIN Check
    PowerOn_Reset_Check();
    // Power on timing control
    PowerOn_Sequencing_Control();    
    // Flash & SRAM Upload
    PowerON_Table_Init();
    //LDD initialize
    MALD38435_TX14_PowerOn_Reset();
    MALD38435_TX14_PowerOn_init();
    // TIA initialize
    MATA38434_RX1_RX4_RESET();
    MATA38434_P8A_Write(); 

    CALIB_MEMORY_MAP.DDMI_DISABLE = 0 ;
    
    while(1)
    {
        // I2C Enable Control
        ModSelL_Function();
        // MSA StateMachine Control
        QSFP28_MSA_StateMachine(); 
        // Luxshare-OET internal control
        MCU_READ_WRITE_DEIVCE_COMMAND_CONTROL(); 
        //HW RESET 
        RESET_L_Function();
        // Move Sram & Flash read/write
        SRMA_Flash_Function();
    }
}

