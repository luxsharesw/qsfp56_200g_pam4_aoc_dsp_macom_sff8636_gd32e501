#include "gd32e501_i2c.h"
#include "Calibration_Struct.h"
#include "Calibration_Struct_2.h"
#include "MA38435_TX1_TX4.h"
#include "MA38434_RX1_RX4.h"
#include "SFF8636_MSA.h"
#include "BRCM_DSP87540_RW.h"
#include <string.h>
#include "BRCM_DSP_87540.h"
#include "GD_Flash_Map_SFF8636.h"
//-----------------------------------------------------------------------------
// Page Select define
//-----------------------------------------------------------------------------
#define BL_ERASE_PAGE0     0x08000000

#define MA38435_T14  	   0x82
#define MA38434_R14        0x8A

#define Calibration        0x90
#define CTLE_EM            0x91
#define Calibration_1      0x92
#define BRCM_87540		   0x85
#define BRCM_87540_LS_PHY0 0x86
#define BRCM_87540_SS_PHY0 0x87

#define ID_Debug       	   0xB0
#define BL_MODE            0xD0

#define Page0              0x00
#define Page1              0x01
#define Page2              0x02
#define Page3              0x03
#define Page20             0x20
#define Page21             0x21
#define Page30             0x30

#define LUT_BIAS0          0xA0
#define LUT_BIAS1          0xA1
#define LUT_BIAS2          0xA2
#define LUT_BIAS3          0xA3

// Luxshare-ICT Password
#define Leve1_PS0   0x93
#define Leve1_PS1   0x78
#define Leve1_PS2   0xCC
#define Leve1_PS3   0xAE

#define Leve2_PS0   0x3A
#define Leve2_PS1   0x18
#define Leve2_PS2   0xB6
#define Leve2_PS3   0x6F

//-----------------------------------------------------------------------------
// ID Debug Info table define
//-----------------------------------------------------------------------------
uint8_t ID_Debug_Info[9] =
{
    'G','D','3','2','E','5','0','1',' ',     // MCU                         // 00,01,02,03,04,05,06,07,08
};
#define ID_Debug_Info_SIZE sizeof(ID_Debug_Info)

uint8_t I2C_Data_Buffer[256] ;
uint8_t MSA_Memory_ADR = 0 ;
uint8_t Data_count_I2C0 = 0;
uint8_t Slave_RW_STOP_Flag = 2 ;
uint8_t HOSTPASSWROD = 0;
uint8_t UPDATE_FLAG;
uint8_t DATA_LEGHT ;
uint8_t I2C_READY_FLG = 0 ;
uint8_t configaddress = 0;

uint8_t SFF8636_PAGE20[128]={0};
uint8_t SFF8636_PAGE21[128]={0};
//-----------------------------------------------------------------------------
// Update SRAM Data Value
//-----------------------------------------------------------------------------
void PassWord_Function()
{
	if ( ( SFF8636_A0[123] == Leve1_PS0 ) &&
		 ( SFF8636_A0[124] == Leve1_PS1 ) &&
		 ( SFF8636_A0[125] == Leve1_PS2 ) &&
		 ( SFF8636_A0[126] == Leve1_PS3 ) )
		HOSTPASSWROD = 1;
	else if(( SFF8636_A0[123] == Leve2_PS0 ) &&
		 ( SFF8636_A0[124] == Leve2_PS1 ) &&
		 ( SFF8636_A0[125] == Leve2_PS2 ) &&
		 ( SFF8636_A0[126] == Leve2_PS3 ) )
		HOSTPASSWROD = 2;
	else
		HOSTPASSWROD = 0;
}

//-----------------------------------------------------------------------------
// Update SRAM Data Value 
//-----------------------------------------------------------------------------
void Update_SRAM_Data()
{
	switch(SFF8636_A0[0x7F])
	{
		case MA38435_T14 :
					if( HOSTPASSWROD >= 1 )
					{
						MALD38435_TX1_TX4_READ_P82();
						memcpy(  &SFF8636_A0[128] , &MATA38435_TX1_TX4_MEMORY_MAP , 128 );
					}
					break;

		case BRCM_87540 :
					if( HOSTPASSWROD > 1 )
					{
						memcpy(  &SFF8636_A0[128] , &BRCM_D87540_DSP_MEMORY_MAP.BRCM_REG_ADDR_0 , 128 );
					}
					break;

		case BRCM_87540_LS_PHY0 :
					if( HOSTPASSWROD >= 1 )
					{
						// Read DSP Data Value
						memcpy(  &SFF8636_A0[128] , &BRCM_87540_DSP_LS_PHY0_MEMORY_MAP , 128 );
					}
					break;

		case BRCM_87540_SS_PHY0 :
					if( HOSTPASSWROD >= 1 )
					{
						// Read DSP Data Value
						memcpy(  &SFF8636_A0[128] , &BRCM_87540_DSP_SS_PHY0_MEMORY_MAP , 128 );
					}
					break;

		case MA38434_R14 :
					if( HOSTPASSWROD >= 1 )
					{
						MATA38434_P8A_READ();
						memcpy(  &SFF8636_A0[128] , &MATA38434_RX1_RX4_TIA_MEMORY_MAP , 128 );
					}
					break;

		case Calibration :

					if( HOSTPASSWROD > 1 )
						memcpy( &SFF8636_A0[128] , &CALIB_MEMORY_MAP, 128);
					else
						memset( &SFF8636_A0[128] , 0xff , 128 ) ;

					break;

		case Calibration_1 :
					if( HOSTPASSWROD > 1 )
                        memcpy( &SFF8636_A0[128] , &CALIB_MEMORY_1_MAP, 128);
					else
						memset( &SFF8636_A0[128] , 0xff , 128 ) ;
                    
					break;

		case CTLE_EM :
					if( HOSTPASSWROD > 1 )
						GDMCU_FMC_READ_FUNCTION( FS_MSA_O_P91 , &SFF8636_A0[128], 128 );
					
					break;
        case LUT_BIAS0:
					if( HOSTPASSWROD > 1 )
						GDMCU_FMC_READ_FUNCTION( FS_Ibias0_PA0 , &SFF8636_A0[128], 128 );            
                    else
						memset( &SFF8636_A0[128] , 0xFF , 128 ) ;
                    break;
                    
        case LUT_BIAS1:
					if( HOSTPASSWROD > 1 )
						GDMCU_FMC_READ_FUNCTION( FS_Ibias1_PA1 , &SFF8636_A0[128], 128 );            
                    else
						memset( &SFF8636_A0[128] , 0xFF , 128 ) ;
                    break;
  
        case LUT_BIAS2:
					if( HOSTPASSWROD > 1 )
						GDMCU_FMC_READ_FUNCTION( FS_Ibias2_PA2 , &SFF8636_A0[128], 128 );            
                    else
						memset( &SFF8636_A0[128] , 0xFF , 128 ) ;
                    break;
                
        case LUT_BIAS3:
					if( HOSTPASSWROD > 1 )
						GDMCU_FMC_READ_FUNCTION( FS_Ibias3_PA3 , &SFF8636_A0[128], 128 );            
                    else
						memset( &SFF8636_A0[128] , 0xFF , 128 ) ;
                    break;   
                    
		case ID_Debug :
					if( HOSTPASSWROD > 1 )
					{
						memcpy( &SFF8636_A0[128] , &ID_Debug_Info , 9 );
				    	GDMCU_FMC_READ_FUNCTION( ( FS_ID_INFO_PB0+9 ) , &SFF8636_A0[137], 119 );
					}
					else
						memset( &SFF8636_A0[128] , 0xff , 128 ) ;

					break;
		default :
						memset( &SFF8636_A0[128] , 0xff , 128 ) ;
			     break;
	}
}

//-----------------------------------------------------------------------------
// Write SRAM to Flash Value
//-----------------------------------------------------------------------------

void Write_SRAM_TO_Flash()
{
	switch(SFF8636_A0[0x7F])
	{
		case Page0 :
					if( HOSTPASSWROD >= 1 )
					{
                        GDMCU_Flash_Erase(FS_SFF8636_A0);
						GDMCU_FMC_BytesWRITE_FUNCTION( FS_SFF8636_A0 , &SFF8636_A0[0]   , 128 );
                        GDMCU_Flash_Erase(FS_SFF8636_P0);
						GDMCU_FMC_BytesWRITE_FUNCTION( FS_SFF8636_P0 , &SFF8636_A0[128]   , 128 );
                        memcpy( &SFF8636_PAGE0[0] , &SFF8636_A0[128]  , 128 );
					}
					break;
		case Page1 :
					if( HOSTPASSWROD >= 1 )
					{
                        GDMCU_Flash_Erase(FS_SFF8636_P1);
						GDMCU_FMC_BytesWRITE_FUNCTION( FS_SFF8636_P1 , &SFF8636_A0[128] , 128 );
                        memcpy( &SFF8636_PAGE1[0] , &SFF8636_A0[128]  , 128 );
					}
					break;

		case Page2 :
					if( HOSTPASSWROD >= 1 )
					{
                        GDMCU_Flash_Erase(FS_SFF8636_P2);
						GDMCU_FMC_BytesWRITE_FUNCTION( FS_SFF8636_P2 , &SFF8636_A0[128]  , 128 );
                        memcpy( &SFF8636_PAGE2[0] , &SFF8636_A0[128]  , 128 );
					}

					break;

		case Page3 :
					if( HOSTPASSWROD >= 1 )
					{
                        GDMCU_Flash_Erase(FS_SFF8636_P3);
						GDMCU_FMC_BytesWRITE_FUNCTION( FS_SFF8636_P3 , &SFF8636_A0[128] , 128 );
						memcpy(  &SFF8636_PAGE3[0] , &SFF8636_A0[128] , 128 );
					}
					else
						memcpy(  &SFF8636_PAGE3[106] , &SFF8636_A0[234] , 8 );

					break;
                    
	    case Page30 :
	    			memcpy(  &SFF8636_PAGE30[0] , &SFF8636_A0[128] , 128 );
	    	        break;

		case MA38435_T14 :
					if( HOSTPASSWROD > 1 )
					{
                        GDMCU_Flash_Erase(FS_VCSEL_P82);
                        GDMCU_FMC_BytesWRITE_FUNCTION( FS_VCSEL_P82 , &SFF8636_A0[128] , 128 );
                        memcpy( &MATA38435_TX1_TX4_MEMORY_MAP.CHIP_ID , &SFF8636_A0[128] , 128);
                        MALD38435_TX1_TX4_WRITE_P82();
					}
					break;

		case BRCM_87540 :
					if( HOSTPASSWROD > 1 )
						memcpy(  &BRCM_D87540_DSP_MEMORY_MAP.BRCM_REG_ADDR_0 , &SFF8636_A0[128] , 128 );
		
					break ;

		case BRCM_87540_LS_PHY0 :
					if( HOSTPASSWROD > 1 )
					{
						// Line-side save flash form SFF8636_A0 byte 128 - 256
                        GDMCU_Flash_Erase(FS_DSP_LS_P86);
						GDMCU_FMC_BytesWRITE_FUNCTION( FS_DSP_LS_P86 , &SFF8636_A0[128] , 128 );
						memcpy(  &BRCM_87540_DSP_LS_PHY0_MEMORY_MAP , &SFF8636_A0[128] , 128 );
					}
					break;

		case BRCM_87540_SS_PHY0 :
					if( HOSTPASSWROD > 1 )
					{
                        GDMCU_Flash_Erase(FS_DSP_SS_P87);
						GDMCU_FMC_BytesWRITE_FUNCTION( FS_DSP_SS_P87 , &SFF8636_A0[128], 128 );
						memcpy(  &BRCM_87540_DSP_SS_PHY0_MEMORY_MAP , &SFF8636_A0[128] , 128 );

					}
					break;

		case MA38434_R14 :
					if( HOSTPASSWROD > 1 )
					{
                        GDMCU_Flash_Erase(FS_TIA_P8A);
                        GDMCU_FMC_BytesWRITE_FUNCTION( FS_TIA_P8A , &SFF8636_A0[128] , 128 );
                        memcpy( &MATA38434_RX1_RX4_TIA_MEMORY_MAP , &SFF8636_A0[128], 128 );
                        MATA38434_P8A_Write();
					}
					break;

		case Calibration :
					if( HOSTPASSWROD > 1 )
					{
						memcpy( &SFF8636_A0[248] , &CALIB_MEMORY_MAP.CHECKSUM_V, 8);
                        GDMCU_Flash_Erase(FS_Cal0_P90);
						GDMCU_FMC_BytesWRITE_FUNCTION( FS_Cal0_P90 , &SFF8636_A0[128] , 128 );
						GDMCU_FMC_READ_FUNCTION( FS_Cal0_P90 , &CALIB_MEMORY_MAP.VCC_SCALEM ,128 );
					}
					break ;

		case Calibration_1 :
					if( HOSTPASSWROD > 1 )
					{
						memcpy( &CALIB_MEMORY_1_MAP , &SFF8636_A0[128] , 128);
					}
					break ;

	    case CTLE_EM :
					if( HOSTPASSWROD > 1 )
					{
                        GDMCU_Flash_Erase(FS_MSA_O_P91);
                        GDMCU_FMC_BytesWRITE_FUNCTION( FS_MSA_O_P91 ,&SFF8636_A0[128], 128 );
                    }
					break ;
        case LUT_BIAS0:
					if( HOSTPASSWROD > 1 )
					{
                        GDMCU_Flash_Erase(FS_Ibias0_PA0);
                        GDMCU_FMC_BytesWRITE_FUNCTION( FS_Ibias0_PA0 ,&SFF8636_A0[128], 128 );
                    }            
                    break;
        case LUT_BIAS1:
					if( HOSTPASSWROD > 1 )
					{
                        GDMCU_Flash_Erase(FS_Ibias1_PA1);
                        GDMCU_FMC_BytesWRITE_FUNCTION( FS_Ibias1_PA1 ,&SFF8636_A0[128], 128 );
                    }            
                    break;
        case LUT_BIAS2:
					if( HOSTPASSWROD > 1 )
					{
                        GDMCU_Flash_Erase(FS_Ibias2_PA2);
                        GDMCU_FMC_BytesWRITE_FUNCTION( FS_Ibias2_PA2 ,&SFF8636_A0[128], 128 );
                    }            
                    break;
        case LUT_BIAS3:
					if( HOSTPASSWROD > 1 )
					{
                        GDMCU_Flash_Erase(FS_Ibias3_PA3);
                        GDMCU_FMC_BytesWRITE_FUNCTION( FS_Ibias3_PA3 ,&SFF8636_A0[128], 128 );
                    }            
                    break;
		case ID_Debug :
					if( HOSTPASSWROD > 1 )
					{
                        GDMCU_Flash_Erase(FS_ID_INFO_PB0);
                        GDMCU_FMC_BytesWRITE_FUNCTION( FS_ID_INFO_PB0 , &SFF8636_A0[128] , 128 );
                    }
					break;
		case BL_MODE :
					if( HOSTPASSWROD > 1 )
					{
                        if( MSA_Memory_ADR == 0x24 )
						{
                            if( (I2C_Data_Buffer[0]==0x11) && (I2C_Data_Buffer[1]==0x22) && (I2C_Data_Buffer[2]==0x33) &&
                                (I2C_Data_Buffer[3]==0x55) && (I2C_Data_Buffer[4]==0x66) && (I2C_Data_Buffer[5]==0x55)	)
                            {
                                //i2c_disable(I2C0);   
                                fwdgt_config( 5 , FWDGT_PSC_DIV64 );
                                fwdgt_enable();                                
                                GDMCU_Flash_Erase(BL_ERASE_PAGE0);
                                //nvic_system_reset();
                            } 
                        }
						
					}
					break;
		default :
			     break;
	}
}

/*!
    \brief      handle I2C0 event interrupt request
    \param[in]  none
    \param[out] none
    \retval     none
*/
void I2C0_EV_IRQHandler(void)
{
    // slave address get pass flag 
    if(i2c_interrupt_flag_get(I2C0, I2C_INT_FLAG_ADDSEND))
    {
        /* clear the ADDSEND bit */
        i2c_interrupt_flag_clear(I2C0, I2C_INT_FLAG_ADDSEND);  
        //configaddress = i2c_recevied_address_get(I2C0)<<1;         
        /* clear I2C_TDATA register */
        I2C_STAT(I2C0) |= I2C_STAT_TBE;
    }
    // Save data / move data 
    else if(i2c_interrupt_flag_get(I2C0, I2C_INT_FLAG_RBNE))
    {
        if(Data_count_I2C0 == 0)
            MSA_Memory_ADR = i2c_data_receive(I2C0);
        else
            I2C_Data_Buffer[Data_count_I2C0-1] = i2c_data_receive(I2C0);

    //    Slave_RW_STOP_Flag = 0 ;
        Data_count_I2C0 ++ ;
    }
    // send data to I2C
    else if(i2c_interrupt_flag_get(I2C0, I2C_INT_FLAG_TI))
    {
        if(MSA_Memory_ADR<128)
        {
            if(( MSA_Memory_ADR > 118 ) && ( MSA_Memory_ADR < 127 ))
                SFF8636_A0[MSA_Memory_ADR] = 0x00;

            i2c_data_transmit(I2C0,SFF8636_A0[MSA_Memory_ADR]);
            
            if( ( MSA_Memory_ADR > 2 ) && ( MSA_Memory_ADR < 15 ) )
                Clear_Flag( MSA_Memory_ADR );   
                        
            MSA_Memory_ADR++;
        }
        else
        {
            if(SFF8636_A0[127]==0x00)
                i2c_data_transmit(I2C0,SFF8636_PAGE0[MSA_Memory_ADR-128]);
            else if(SFF8636_A0[127]==0x01)
                i2c_data_transmit(I2C0,SFF8636_PAGE1[MSA_Memory_ADR-128]);
            else if(SFF8636_A0[127]==0x02)
                i2c_data_transmit(I2C0,SFF8636_PAGE2[MSA_Memory_ADR-128]);
            else if(SFF8636_A0[127]==0x03)
                i2c_data_transmit(I2C0,SFF8636_PAGE3[MSA_Memory_ADR-128]);
            else if(SFF8636_A0[127]==0x30)
                i2c_data_transmit(I2C0,SFF8636_PAGE30[MSA_Memory_ADR-128]);
            else
                i2c_data_transmit(I2C0,SFF8636_A0[MSA_Memory_ADR]);
            MSA_Memory_ADR++;
        }         
    //    Slave_RW_STOP_Flag = 1 ;
    }
    // Stop flag clear
    else if(i2c_interrupt_flag_get(I2C0, I2C_INT_FLAG_STPDET))
    {
        if( Data_count_I2C0 >= 2 )
        {
            I2C_READY_FLG = 1 ;
            DATA_LEGHT = ( Data_count_I2C0 - 1 );
            if(( MSA_Memory_ADR == 0x7F ) && (Data_count_I2C0 == 2))
            {
                SFF8636_A0[127]=I2C_Data_Buffer[0];
                
                if(SFF8636_A0[127]==0x00)
                {
                    memcpy(  &SFF8636_A0[128] , &SFF8636_PAGE0[0] , 128 );
                    UPDATE_FLAG = 0 ;
                }
                else if(SFF8636_A0[127]==0x01)
                {
                    memcpy(  &SFF8636_A0[128] , &SFF8636_PAGE1[0] , 128 );
                    UPDATE_FLAG = 0 ;
                }
                else if(SFF8636_A0[127]==0x02)
                {
                    memcpy(  &SFF8636_A0[128] , &SFF8636_PAGE2[0] , 128 );
                    UPDATE_FLAG = 0 ;
                }
                else if(SFF8636_A0[127]==0x03)
                {
                    memcpy(  &SFF8636_A0[128] , &SFF8636_PAGE3[0] , 128 );
                    UPDATE_FLAG = 0 ;
                }
                else if(SFF8636_A0[127]==0x20)
                {
                    memcpy(  &SFF8636_A0[128] , &SFF8636_PAGE20[0] , 128 );
                    UPDATE_FLAG = 0 ;
                }
                else if(SFF8636_A0[127]==0x21)
                {
                    memcpy(  &SFF8636_A0[128] , &SFF8636_PAGE21[0] , 128 );
                    UPDATE_FLAG = 0 ;
                }
                else if(SFF8636_A0[127]==0x30)
                {
                    memcpy(  &SFF8636_A0[128] , &SFF8636_PAGE30[0] , 128 );
                    UPDATE_FLAG = 0 ;
                }
                //For Password auto check use on gui
                else if(SFF8636_A0[127]==0x90)
                {
                    UPDATE_FLAG = 1;
                }
                else
                {
                    if(HOSTPASSWROD>1)
                    {
                        UPDATE_FLAG = 1;
                    }
                    else
                    {
                        GDMCU_FMC_READ_FUNCTION( FS_SFF8636_P0 , &SFF8636_PAGE0[0] , 128 );
                        SFF8636_A0[127]=0x00;
                        UPDATE_FLAG = 0;
                    }
                }

            }
            // Fix PW not change when get invalid value
            else if( ( MSA_Memory_ADR >= 122 ) && ( MSA_Memory_ADR <= 126 ))
                UPDATE_FLAG = 5;
            else
            {
                if( HOSTPASSWROD >= 1 )
                    memcpy(&SFF8636_A0[MSA_Memory_ADR], &I2C_Data_Buffer[0], DATA_LEGHT);
                else
                {
                    if(MSA_Memory_ADR==86)
                        SFF8636_A0[MSA_Memory_ADR] = I2C_Data_Buffer[0];
                    else if(MSA_Memory_ADR==87)
                        SFF8636_A0[MSA_Memory_ADR] = I2C_Data_Buffer[0];
                    else if(MSA_Memory_ADR==88)
                        SFF8636_A0[MSA_Memory_ADR] = I2C_Data_Buffer[0];
                    else if(MSA_Memory_ADR==93)
                        SFF8636_A0[MSA_Memory_ADR] = I2C_Data_Buffer[0];
                }

                UPDATE_FLAG = 7 ;
            }
        }
        /* clear STPDET interrupt flag */
        i2c_interrupt_flag_clear(I2C0, I2C_INT_FLAG_STPDET);
        Data_count_I2C0 = 0 ;
        //Slave_RW_STOP_Flag = 2 ;
    }
    if(i2c_flag_get(I2C0,I2C_FLAG_NACK))
    {
        MSA_Memory_ADR = MSA_Memory_ADR-1 ;
        i2c_flag_clear(I2C0,I2C_FLAG_NACK);
    }
}

/*!
    \brief      handle I2C0 error interrupt request
    \param[in]  none
    \param[out] none
    \retval     none
*/
void I2C0_ER_IRQHandler(void)
{
    /* bus error */
    if(i2c_interrupt_flag_get(I2C0, I2C_INT_FLAG_BERR)){
        i2c_interrupt_flag_clear(I2C0, I2C_INT_FLAG_BERR);
        i2c_deinit(I2C0);
    }

    /* arbitration lost */
    if(i2c_interrupt_flag_get(I2C0, I2C_INT_FLAG_LOSTARB)){
        i2c_interrupt_flag_clear(I2C0, I2C_INT_FLAG_LOSTARB);
    }

    /* over-run or under-run when SCL stretch is disabled */
    if(i2c_interrupt_flag_get(I2C0, I2C_INT_FLAG_OUERR)){
        i2c_interrupt_flag_clear(I2C0, I2C_INT_FLAG_OUERR);
    }

    /* PEC error */
    if(i2c_interrupt_flag_get(I2C0, I2C_INT_FLAG_PECERR)){
       i2c_interrupt_flag_clear(I2C0, I2C_INT_FLAG_PECERR);
    }

    /* timeout error */
    if(i2c_interrupt_flag_get(I2C0, I2C_INT_FLAG_TIMEOUT)){
        i2c_interrupt_flag_clear(I2C0, I2C_INT_FLAG_TIMEOUT);
        i2c_deinit(I2C0);
    }

    /* SMBus alert */
    if(i2c_interrupt_flag_get(I2C0, I2C_INT_FLAG_SMBALT)){
        i2c_interrupt_flag_clear(I2C0, I2C_INT_FLAG_SMBALT);
    }

    /* disable the I2C0 interrupt */
    //i2c_interrupt_disable(I2C0, I2C_INT_ERR | I2C_INT_STPDET | I2C_INT_TI | I2C_INT_TC);
}


void SRMA_Flash_Function()
{
	uint16_t Data_count ;

	if( I2C_READY_FLG==1 )
	{
		switch(UPDATE_FLAG)
		{
			case 1 :
				    Update_SRAM_Data();
					break ;

			case 5 :
					for( Data_count=0 ; Data_count < DATA_LEGHT ; Data_count++ )
						SFF8636_A0[ ( MSA_Memory_ADR + Data_count ) ] = I2C_Data_Buffer[Data_count];
					PassWord_Function();
					break ;

			case 7 :
					Write_SRAM_TO_Flash();
					break ;
			default :
				break ;
		}

		I2C_READY_FLG = 0 ;
	}

}
