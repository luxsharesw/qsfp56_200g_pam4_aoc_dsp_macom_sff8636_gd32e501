#include "gd32e501.h"
#include "core_cm33.h"
#include "systick.h"
#include "system_gd32e501.h"

__IO uint32_t prescaler_a = 0, prescaler_s = 0;
//----------------------------------------//
// Rtc pre Config
//----------------------------------------//
void rtc_pre_config(void)
{
    rcu_rtc_clock_config(RCU_RTCSRC_IRC40K);
    prescaler_s = 0x18F;
    prescaler_a = 0x63; 
    rcu_periph_clock_enable(RCU_RTC);   
    rtc_register_sync_wait();    
}
//----------------------------------------//
// Ruc periph clock initialize
//----------------------------------------//
void irc40k_config(void)
{
    /* enable IRC40K */
    rcu_osci_on(RCU_IRC40K);
    /* wait till IRC40K is ready */
    rcu_osci_stab_wait(RCU_IRC40K);
}

void GD_System_Periph_Clock_Enable()
{
    //System CLK 
    systick_config();
    irc40k_config();
    // GPIO CLK
    rcu_periph_clock_enable(RCU_GPIOA);
    rcu_periph_clock_enable(RCU_GPIOB);
    rcu_periph_clock_enable(RCU_GPIOC);
    rcu_periph_clock_enable(RCU_GPIOF);
    // CFGCMP CLK 
    rcu_periph_clock_enable(RCU_CFGCMP);
    // ADC CLK
    rcu_periph_clock_enable(RCU_ADC);
    rcu_adc_clock_config(RCU_ADCCK_APB2_DIV6);
    // DAC CLK for FR4 
    //rcu_periph_clock_enable(RCU_DAC0);
    //rcu_periph_clock_enable(RCU_DAC1);
    //rcu_periph_clock_enable(RCU_DAC2);
    //rcu_periph_clock_enable(RCU_DAC3);

    // Interface rcu clk enable 
    rcu_periph_reset_enable(RCU_I2C1RST);  
    rcu_periph_reset_disable(RCU_I2C1RST);    
    rcu_periph_reset_enable(RCU_I2C2RST);
    rcu_periph_reset_disable(RCU_I2C2RST);   
    rcu_periph_clock_enable(RCU_I2C1);
    rcu_periph_clock_enable(RCU_I2C2);
}
//----------------------------------------//
// GPIO initialize
// Digital op
//----------------------------------------//
// PA2  Digital OP  - MDC 
// PA3  Digital OP  - MDIO 
// PA4  Digital OP  - SS_N 
// PA5  Digital OP  - SCK
// PA6  Digital OP  - MISO
// PA7  Digital OP  - MOSI
// PB15 Digital OP  - INTR_N DSP
// PC9  Digital OP  - IntL_G
// PC15 Digital OP  - ModselL_G
// PF0  Digital OP  - LPMODE_G
// PF1  Digital OP  - RESETL_G
void GD32E501_GPIO_OP_initial()
{
    gpio_mode_set(GPIOA, GPIO_MODE_INPUT, GPIO_PUPD_NONE, GPIO_PIN_2);
    gpio_mode_set(GPIOA, GPIO_MODE_INPUT, GPIO_PUPD_NONE, GPIO_PIN_3);
    gpio_mode_set(GPIOA, GPIO_MODE_INPUT, GPIO_PUPD_NONE, GPIO_PIN_4);
    gpio_mode_set(GPIOA, GPIO_MODE_INPUT, GPIO_PUPD_NONE, GPIO_PIN_5);    
    gpio_mode_set(GPIOA, GPIO_MODE_INPUT, GPIO_PUPD_NONE, GPIO_PIN_6);
    gpio_mode_set(GPIOA, GPIO_MODE_INPUT, GPIO_PUPD_NONE, GPIO_PIN_7); 
    gpio_mode_set(GPIOB, GPIO_MODE_INPUT, GPIO_PUPD_NONE, GPIO_PIN_15); 
    gpio_mode_set(GPIOC, GPIO_MODE_INPUT, GPIO_PUPD_NONE, GPIO_PIN_15); 
    gpio_mode_set(GPIOF, GPIO_MODE_INPUT, GPIO_PUPD_NONE, GPIO_PIN_0);  
    gpio_mode_set(GPIOF, GPIO_MODE_INPUT, GPIO_PUPD_NONE, GPIO_PIN_1); 
    // IntLG ouput open drain
    gpio_mode_set(GPIOC, GPIO_MODE_OUTPUT, GPIO_PUPD_NONE, GPIO_PIN_9);    
}
//----------------------------------------//
// GPIO initialize
// Digital pp
//----------------------------------------//
// PA11 Digital PP  - P3V3_DSP_EN
// PA12 Digital PP  - P3V3_RX_EN
// PA15 Digital PP  - P3V3_TX_EN
// PB9  Digital PP  - P1V8_TX_EN
// PB12 Digital PP  - MODSELB DSP
// PB13 Digital PP  - RESET_L DSP
// PB14 Digital PP  - LPMODE DSP
// PC6  Digital PP  - OSC_EN

void GD32E501_GPIO_PP_initial()
{
    gpio_mode_set(GPIOA, GPIO_MODE_OUTPUT, GPIO_PUPD_PULLDOWN, GPIO_PIN_11); 
    gpio_mode_set(GPIOA, GPIO_MODE_OUTPUT, GPIO_PUPD_PULLDOWN, GPIO_PIN_12); 
    gpio_mode_set(GPIOA, GPIO_MODE_OUTPUT, GPIO_PUPD_PULLDOWN, GPIO_PIN_15);    
    gpio_mode_set(GPIOB, GPIO_MODE_OUTPUT, GPIO_PUPD_PULLDOWN, GPIO_PIN_9); 
    gpio_mode_set(GPIOB, GPIO_MODE_OUTPUT, GPIO_PUPD_PULLUP, GPIO_PIN_12); 
    gpio_mode_set(GPIOB, GPIO_MODE_OUTPUT, GPIO_PUPD_PULLUP, GPIO_PIN_13);
    gpio_mode_set(GPIOB, GPIO_MODE_OUTPUT, GPIO_PUPD_PULLUP, GPIO_PIN_14);  
    gpio_mode_set(GPIOC, GPIO_MODE_OUTPUT, GPIO_PUPD_PULLUP, GPIO_PIN_6);      
//    gpio_output_options_set(GPIOA, GPIO_PUPD_PULLUP, GPIO_OSPEED_2MHZ, GPIO_PIN_11);
//    gpio_output_options_set(GPIOA, GPIO_PUPD_PULLUP, GPIO_OSPEED_2MHZ, GPIO_PIN_12);
//    gpio_output_options_set(GPIOA, GPIO_PUPD_PULLUP, GPIO_OSPEED_2MHZ, GPIO_PIN_15);  
//    gpio_output_options_set(GPIOB, GPIO_PUPD_PULLUP, GPIO_OSPEED_2MHZ, GPIO_PIN_9);
//    gpio_output_options_set(GPIOB, GPIO_PUPD_PULLUP, GPIO_OSPEED_2MHZ, GPIO_PIN_12);
//    gpio_output_options_set(GPIOB, GPIO_PUPD_PULLUP, GPIO_OSPEED_2MHZ, GPIO_PIN_13);
//    gpio_output_options_set(GPIOB, GPIO_PUPD_PULLUP, GPIO_OSPEED_2MHZ, GPIO_PIN_14);
//    gpio_output_options_set(GPIOC, GPIO_PUPD_PULLUP, GPIO_OSPEED_2MHZ, GPIO_PIN_6);
}

//----------------------------------------//
// GPIO initialize
// Analogy
//----------------------------------------//
// PA1  Analogy     - P1V8 Monitor     - ADC_IN1
// PB0  Analogy     - P3V3 Rx Monitor  - ADC_IN8
// PB2  Analogy     - P3V3 Tx Monitor  - ADC_IN4
// PB5  Analogy     - RSSI Monitor     - ADC_IN7
// PC0  Analogy     - MPD1 Monitor     - ADC_IN10
// PC1  Analogy     - MPD2 Monitor     - ADC_IN11
// PC2  Analogy     - MPD3 Monitor     - ADC_IN12
// PC3  Analogy     - MPD4 Monitor     - ADC_IN13
void GD32E501_GPIO_Analogy_initial()
{
    gpio_mode_set(GPIOA, GPIO_MODE_ANALOG, GPIO_PUPD_NONE, GPIO_PIN_1);
    gpio_mode_set(GPIOB, GPIO_MODE_ANALOG, GPIO_PUPD_NONE, GPIO_PIN_0); 
    gpio_mode_set(GPIOB, GPIO_MODE_ANALOG, GPIO_PUPD_NONE, GPIO_PIN_2); 
    gpio_mode_set(GPIOB, GPIO_MODE_ANALOG, GPIO_PUPD_NONE, GPIO_PIN_5); 
    gpio_mode_set(GPIOC, GPIO_MODE_ANALOG, GPIO_PUPD_NONE, GPIO_PIN_0); 
    gpio_mode_set(GPIOC, GPIO_MODE_ANALOG, GPIO_PUPD_NONE, GPIO_PIN_1); 
    gpio_mode_set(GPIOC, GPIO_MODE_ANALOG, GPIO_PUPD_NONE, GPIO_PIN_2); 
    gpio_mode_set(GPIOC, GPIO_MODE_ANALOG, GPIO_PUPD_NONE, GPIO_PIN_3);     
}
//-----------------------------------------------//
// Interface
// I2C
//-----------------------------------------------//
// I2C  Master
// PB3  Digital OP  - SCL_LDD
// PB4  Digital OP  - SDA_LDD
// PC7  Digital OP  - SCL_TIA
// PC8  Digital OP  - SDA_TIA
void Master_I2C1_PB34_Initial()
{
    //rcu_periph_reset_enable(RCU_I2C1RST);
    //rcu_periph_reset_disable(RCU_I2C1RST);
    /* enable GPIOB clock */
    //rcu_periph_clock_enable(RCU_GPIOB);
    /* enable BOARD_I2C APB1 clock */
    //rcu_periph_clock_enable(RCU_I2C1);
    
    /* connect PB3 to I2C1_SCL */
    gpio_af_set(GPIOB, GPIO_AF_7, GPIO_PIN_3);
    /* connect PB4 to I2C1_SDA */
    gpio_af_set(GPIOB, GPIO_AF_7, GPIO_PIN_4);
    
    /* configure  I2C2 GPIO */
    gpio_mode_set(GPIOB, GPIO_MODE_AF, GPIO_PUPD_NONE, GPIO_PIN_3);
    gpio_output_options_set(GPIOB, GPIO_OTYPE_OD, GPIO_OSPEED_50MHZ, GPIO_PIN_3); 
    gpio_mode_set(GPIOB, GPIO_MODE_AF, GPIO_PUPD_NONE, GPIO_PIN_4);
    gpio_output_options_set(GPIOB, GPIO_OTYPE_OD, GPIO_OSPEED_50MHZ, GPIO_PIN_4);
      
    /* configure I2C timing */
    i2c_timing_config(I2C1,0,0x9,0);
    i2c_master_clock_config(I2C1,0x78,0x78);
    i2c_address_config(I2C1,0x82,I2C_ADDFORMAT_7BITS);
    /* send slave address to I2C bus */
    //i2c_master_addressing(I2C1,Device_ADR,I2C_MASTER_TRANSMIT);
    /* enable I2Cx */
    i2c_enable(I2C1);	   
}

void Master_I2C2_PC78_Initial()
{
    //rcu_periph_reset_enable(RCU_I2C2RST);
    //rcu_periph_reset_disable(RCU_I2C2RST);
    /* enable GPIOB clock */
    //rcu_periph_clock_enable(RCU_GPIOC);
    /* enable BOARD_I2C APB1 clock */
    //rcu_periph_clock_enable(RCU_I2C2);
    
    /* connect PB10 to I2C1_SCL */
    gpio_af_set(GPIOC, GPIO_AF_1, GPIO_PIN_7);
    /* connect PB11 to I2C1_SDA */
    gpio_af_set(GPIOC, GPIO_AF_1, GPIO_PIN_8);
    
    /* configure  I2C2 GPIO */
    gpio_mode_set(GPIOC, GPIO_MODE_AF, GPIO_PUPD_NONE, GPIO_PIN_7);
    gpio_output_options_set(GPIOC, GPIO_OTYPE_OD, GPIO_OSPEED_50MHZ, GPIO_PIN_7);
    gpio_mode_set(GPIOC, GPIO_MODE_AF, GPIO_PUPD_NONE, GPIO_PIN_8);
    gpio_output_options_set(GPIOC, GPIO_OTYPE_OD, GPIO_OSPEED_50MHZ, GPIO_PIN_8);
    
    /* configure I2C timing */
    i2c_timing_config(I2C2,0,0x9,0);
    i2c_master_clock_config(I2C2,0x78,0x78);    // 400 KHz
    //i2c_master_clock_config(I2C2,0x9C,0x9c);  // 100 KHz
    i2c_address_config(I2C2,0x82,I2C_ADDFORMAT_7BITS);
    /* send slave address to I2C bus */
    i2c_master_addressing(I2C2,0xA0,I2C_MASTER_TRANSMIT);
    /* enable I2Cx */
    i2c_enable(I2C2);	
}
// I2C Slave
// PB6  Digital OP  - SCL_G
// PB7  Digital OP  - SDA_G
void Slave_I2C0_PB67_Initial()
{
    rcu_periph_reset_enable(RCU_I2C0RST);
    rcu_periph_reset_disable(RCU_I2C0RST);
    /* enable GPIOB clock */
    /* enable I2C0 clock */
    rcu_periph_clock_enable(RCU_I2C0);
    /* connect PB6 to I2C0_SCL */
    gpio_af_set(GPIOB, GPIO_AF_1, GPIO_PIN_6);
    /* connect PB7 to I2C0_SDA */
    gpio_af_set(GPIOB, GPIO_AF_1, GPIO_PIN_7);
    /* configure GPIO pins of I2C0 */
    gpio_mode_set(GPIOB, GPIO_MODE_AF, GPIO_PUPD_PULLUP, GPIO_PIN_6);
    gpio_output_options_set(GPIOB, GPIO_OTYPE_OD, GPIO_OSPEED_50MHZ, GPIO_PIN_6);
    gpio_mode_set(GPIOB, GPIO_MODE_AF, GPIO_PUPD_PULLUP, GPIO_PIN_7);
    gpio_output_options_set(GPIOB, GPIO_OTYPE_OD, GPIO_OSPEED_50MHZ, GPIO_PIN_7);
    
    /* configure I2C timing , delay time to max*/
    i2c_timing_config(I2C0,0,0x0F,0);
    
    /* configure I2C address */
    i2c_address_config(I2C0, 0xA0, I2C_ADDFORMAT_7BITS);
    /* configure number of bytes to be transferred */
    /* enable I2C reload mode */
    i2c_reload_enable(I2C0);
    // Setting Nvic irq
    nvic_irq_enable(I2C0_EV_IRQn, 2);
    nvic_irq_enable(I2C0_ER_IRQn, 1);
    
    i2c_idle_clock_timeout_config( I2C0 , BUSTOA_DETECT_IDLE );
    
    /* enable the I2C0 interrupt */
    i2c_interrupt_enable(I2C0, I2C_INT_ERR | I2C_INT_STPDET | I2C_INT_ADDM | I2C_INT_RBNE | I2C_INT_TI);
}

void ADC_Config_Initial()
{
    /* ADC temperature and Vrefint enable */
    adc_tempsensor_vrefint_enable();
    /* ADC continuous function enable */
    adc_special_function_config(ADC_SCAN_MODE, ENABLE);
    /* ADC trigger config */
    adc_external_trigger_source_config(ADC_INSERTED_CHANNEL, ADC_EXTTRIG_INSERTED_NONE); 
    /* ADC data alignment config */
    adc_data_alignment_config(ADC_DATAALIGN_RIGHT);
    /* ADC channel length config */
    adc_channel_length_config(ADC_INSERTED_CHANNEL, 0U);
    /* ADC external trigger enable */
    adc_external_trigger_config(ADC_INSERTED_CHANNEL, ENABLE);    
    /* enable ADC interface */
    adc_enable();  
    
    delay_1ms(1U);
    /* ADC calibration and reset calibration */
    adc_calibration_enable();
}


//----------------------------------------//
// GD32E501 Power On Initial
//----------------------------------------//
void GD32E501_Power_on_Initial()
{
    // Enable SR4 Function periph clk
    // GPIO , ADC , I2C , DAC , CFGCMP
    GD_System_Periph_Clock_Enable();
    // GPIo initial
    GD32E501_GPIO_OP_initial();
    GD32E501_GPIO_PP_initial();
    GD32E501_GPIO_Analogy_initial();
    // VREF Enable
    syscfg_vref_enable();
    // ADC Initialize
    ADC_Config_Initial();
    // Interface initial  
    Master_I2C1_PB34_Initial();
    Master_I2C2_PC78_Initial();
    Slave_I2C0_PB67_Initial();
}






