#include "gd32e501.h"
#include "core_cm33.h"
#include "systick.h"
#include "BRCM_DSP87540_RW.h"
#include "BRCM_DSP_87540.h"
#include "Calibration_Struct.h"
#include "Calibration_Struct_2.h"
#include "SFF8636_MSA.h"
#include "MCU_GPIO_Customize_Define.h"

uint8_t RESET_Count = 0;

void Delay_for_Loop(uint16_t DataCount)
{
    uint16_t IIcount,JJcount;
    for(IIcount=0;IIcount<DataCount;DataCount++)
        for(JJcount=0;JJcount<3000;JJcount++);
}

void Internal_Voltage_Power_Down()
{
    // PA11 Digital PP  - P3V3_DSP_EN
    // PA12 Digital PP  - P3V3_RX_EN
    // PA15 Digital PP  - P3V3_TX_EN
    // PB9  Digital PP  - P1V8_TX_EN
    P3V3_DSP_EN_Low();
    P3V3_RX_EN_Low();
    P3V3_TX_EN_Low();
    P1V8_TX_EN_Low();
}

void P1V8_PWM_EN()
{
    uint16_t iicont = 0;
	uint16_t High_PON = 99;
	uint16_t holdTiming = 500;
	while(1)
	{
		iicont++;
		if(iicont<=High_PON)
			P1V8_TX_EN_Low();
		else
		{
			P1V8_TX_EN_High();
			if(iicont>=100)
			{
				if(holdTiming>=10)
				{
					iicont = 0;
					holdTiming--;
				}
				else
				{
					iicont = 0;
					holdTiming = 500;
					High_PON -- ;
					if(High_PON<=95)
						break;
				}
			}
		}
	}
}

void Internal_Voltage_Power_up_Seq()
{
    // PA11 Digital PP  - P3V3_DSP_EN
    // PA12 Digital PP  - P3V3_RX_EN
    // PA15 Digital PP  - P3V3_TX_EN
    // PB9  Digital PP  - P1V8_TX_EN
    delay_1ms(5);
    P3V3_DSP_EN_High();
    delay_1ms(5);
    P3V3_RX_EN_High();
    delay_1ms(5);
    P3V3_TX_EN_High();
    delay_1ms(5);
    P1V8_PWM_EN();
    delay_1ms(1);
}

void DSP_PowerON_Seq_Control()
{
    // PB12 Digital PP  - MODSELB DSP
    // PB13 Digital PP  - RESET_L DSP
    // PB14 Digital PP  - LPMODE DSP
    // PC6  Digital PP  - OSC_EN

    // OSC Enable ( low active )
    OSC_EN_DSP_Low();
    // DSP RESET is Low
    RESET_L_DSP_Low();
    delay_1ms(50);  
    // DSP LPMODE ( High active )
    LPMODE_DSP_High();
    // MODESELB Low is I2C intface
    MODSELB_DSP_Low();
    delay_1ms(1);  
    //DSP unReset , DSP Download SPI EEPROM Image
    RESET_L_DSP_High();
    //Setting is High power mode
    //LPMODE_DSP_Low();
}

void PowerOn_Sequencing_Control()
{
    // Device RESET POR
    Internal_Voltage_Power_Down();
    // Device Power on POR
    Internal_Voltage_Power_up_Seq();
    // DSP Power on for Spi eepeorm fw upgrade
    DSP_PowerON_Seq_Control();
}


uint8_t Get_Power_C_Status()
{
	// bit 0 P3V3_DSP_EN_GPIOA
	// bit 1 P3V3_RX_EN_GPIOA
	// bit 2 P3V3_TX_EN_GPIOA
	// bit 3 P1V8_TX_EN_GPIOB
	// bit 4 OSC_EN_GPIOC
	// bit 5 RESET_L_DSP_GPIOB
	// bit 6 LPMODE_DSP_GPIOB
	// bit 7 MODSELB_DSP_GPIOB
	uint8_t Power_Status = 0 ;
	uint8_t Temp_data = 0 ;

	Temp_data = gpio_output_bit_get(GPIOA, P3V3_DSP_EN_GPIOA) ;
	Power_Status = Power_Status + Temp_data ;
	Temp_data = gpio_output_bit_get(GPIOA, P3V3_RX_EN_GPIOA) ;
	Power_Status = Power_Status + ( Temp_data << 1 );
	Temp_data = gpio_output_bit_get(GPIOA, P3V3_TX_EN_GPIOA) ;
	Power_Status = Power_Status + ( Temp_data << 2 );
	Temp_data = gpio_output_bit_get(GPIOB, P1V8_TX_EN_GPIOB) ;
	Power_Status = Power_Status + ( Temp_data << 3 );   
	Temp_data = gpio_output_bit_get(GPIOC, OSC_EN_GPIOC) ;
	Power_Status = Power_Status + ( Temp_data << 4 ); 
	Temp_data = gpio_output_bit_get(GPIOB, RESET_L_DSP_GPIOB) ;
	Power_Status = Power_Status + ( Temp_data << 5 );    
	Temp_data = gpio_output_bit_get(GPIOB, LPMODE_DSP_GPIOB) ;
	Power_Status = Power_Status + ( Temp_data << 6 );   
	Temp_data = gpio_output_bit_get(GPIOB, MODSELB_DSP_GPIOB) ;
	Power_Status = Power_Status + ( Temp_data << 7 );

	return Power_Status;
}

void SET_Power_Control(uint8_t SET_Value)
{    
    // bit 0 P3V3_DSP_EN_GPIOA
	// bit 1 P3V3_RX_EN_GPIOA
	// bit 2 P3V3_TX_EN_GPIOA
	// bit 3 P1V8_TX_EN_GPIOB
	// bit 4 OSC_EN_GPIOC
	// bit 5 RESET_L_DSP_GPIOB
	// bit 6 LPMODE_DSP_GPIOB
	// bit 7 MODSELB_DSP_GPIOB
	if( ( SET_Value & 0x01 ) == 0x01 )
		P3V3_DSP_EN_High();
	else
		P3V3_DSP_EN_Low();

	if( ( SET_Value & 0x02 ) == 0x02 )
		P3V3_RX_EN_High();
	else
		P3V3_RX_EN_Low();

	if( ( SET_Value & 0x04 ) == 0x04 )
		P3V3_TX_EN_High();
	else
		P3V3_TX_EN_Low();

	if( ( SET_Value & 0x08 ) == 0x08 )
		P1V8_TX_EN_High();
	else
		P1V8_TX_EN_Low();

	if( ( SET_Value & 0x10 ) == 0x10 )
		OSC_EN_DSP_High();
	else
		OSC_EN_DSP_Low();

	if( ( SET_Value & 0x20 ) == 0x20 )
		RESET_L_DSP_High();
	else
		RESET_L_DSP_Low();

	if( ( SET_Value & 0x40 ) == 0x40 )
		LPMODE_DSP_High();
	else
		LPMODE_DSP_Low();

	if( ( SET_Value & 0x80 ) == 0x80 )
		MODSELB_DSP_High();
	else
		MODSELB_DSP_Low();
}

void MCU_READ_WRITE_DEIVCE_COMMAND_CONTROL()
{
    TEST_DSP_Command_Direct_Control();
	// Power Pin Control
	if( CALIB_MEMORY_1_MAP.Power_SET_Start == 0xAA )
	{
		CALIB_MEMORY_1_MAP.Power_SET_Start = 0x00;
		SET_Power_Control( CALIB_MEMORY_1_MAP.Power_SET_Value );
	}
    
    if(CALIB_MEMORY_MAP.DEBUG_TEMP1 == 0xAA)
    {
        CALIB_MEMORY_MAP.DEBUG_TEMP1 = 0x00;
        Get_Selected_Chip_Mode();
        // Disable I2C
        i2c_disable(I2C0);
        DSP87540_Init(DSP_MODE_SET);
        // Enable I2C
        i2c_enable(I2C0); 
    }
    // MCU Flash CheckSum Calculation
	if( CALIB_MEMORY_MAP.CheckSum_EN == 0xAA )
	{
        i2c_disable(I2C0); 
		CheckSum_Calculate();
		CALIB_MEMORY_MAP.CheckSum_EN = 0x00 ;
        i2c_enable(I2C0);
	}
    
    if( BRCM_D87540_DSP_MEMORY_MAP.Trigger_CMD == 0xAA )
	{
		Trigger_CMD_Update_DSP_REG();
        //DSP87540_SystemSide_TRX_Polarity_SET( 0 , TX_Side ); 
		BRCM_D87540_DSP_MEMORY_MAP.Trigger_CMD = 0x00 ;
	}
}

// PC15 Digital OP  - ModselL_G
void ModSelL_Function()
{
	if( gpio_input_bit_get(GPIOC, ModselL_G_GPIOC) == 0 )
		i2c_enable(I2C0);
	else
		i2c_disable(I2C0);  
}


void PowerOn_Reset_Check()
{
    RESET_Count = 0 ;
    //ResetL Function
    if( gpio_input_bit_get(GPIOF, RESETL_G_GPIOF) == 0)
    {
        while(1)
        {
            if( gpio_input_bit_get(GPIOF, RESETL_G_GPIOF) == 1)  
                RESET_Count ++;
            if(RESET_Count>10)
                break;
        }
    }
}

void RESET_L_Function()
{
    if( gpio_input_bit_get(GPIOF, RESETL_G_GPIOF) == 0)
        RESET_Count ++ ;
    
    if( RESET_Count > 10 )
        nvic_system_reset();      
}
