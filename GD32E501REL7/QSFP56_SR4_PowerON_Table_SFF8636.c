#include "gd32e501.h"
#include "core_cm33.h"
#include "string.h"
#include "GD_Flash_Map_SFF8636.h"
#include "BRCM_DSP_87540.h"
#include "MA38435_TX1_TX4.h"
#include "MA38434_RX1_RX4.h"
#include "Calibration_Struct.h"
#include "Calibration_Struct_2.h"
#include "SFF8636_MSA.h"

uint16_t MCU_FW_VERSION = 0x0228;

uint8_t SFF8636_A0[256];
uint8_t SFF8636_PAGE0[128];
uint8_t SFF8636_PAGE1[128];
uint8_t SFF8636_PAGE2[128];
uint8_t SFF8636_PAGE3[128];
uint8_t SFF8636_PAGE30[128];
uint8_t CTEL_Table[16];
uint8_t Pre_Table[16];
uint8_t OP_AMP_Table[16];
uint8_t Post_Table[16];
uint8_t PW_LEVE1[4];
// Tx Disable is Power up
uint8_t Bef_TxDIS_Power=0x00;
// Tx output is enable
uint8_t Bef_Tx_output=0x00;

struct CALIB_MEMORY CALIB_MEMORY_MAP;
struct CALIB_MEMORY_1 CALIB_MEMORY_1_MAP;

void Get_NRZ_Default_System_Side()
{
    // RX Pre-EM

    BRCM_87540_DSP_SS_PHY0_MEMORY_MAP.System_Side_PRE1_CH0=0xF4FF;
    BRCM_87540_DSP_SS_PHY0_MEMORY_MAP.System_Side_PRE1_CH1=0xF4FF;
    BRCM_87540_DSP_SS_PHY0_MEMORY_MAP.System_Side_PRE1_CH2=0xF4FF;
    BRCM_87540_DSP_SS_PHY0_MEMORY_MAP.System_Side_PRE1_CH3=0xF4FF;

    // RX Post-EM

    BRCM_87540_DSP_SS_PHY0_MEMORY_MAP.System_Side_POST1_CH0 = 0x0000;
    BRCM_87540_DSP_SS_PHY0_MEMORY_MAP.System_Side_POST1_CH1 = 0x0000;
    BRCM_87540_DSP_SS_PHY0_MEMORY_MAP.System_Side_POST1_CH2 = 0x0000;
    BRCM_87540_DSP_SS_PHY0_MEMORY_MAP.System_Side_POST1_CH3 = 0x0000;

    // Rx AMP

    BRCM_87540_DSP_SS_PHY0_MEMORY_MAP.System_Side_Main_CH0 = 0x7000;
    BRCM_87540_DSP_SS_PHY0_MEMORY_MAP.System_Side_Main_CH1 = 0x7000;
    BRCM_87540_DSP_SS_PHY0_MEMORY_MAP.System_Side_Main_CH2 = 0x7000;
    BRCM_87540_DSP_SS_PHY0_MEMORY_MAP.System_Side_Main_CH3 = 0x7000;
}
void CheckSum_Calculate()
{
	uint16_t TEMP_BUF=0;
	uint32_t IIcount;
    uint16_t CHECKSUM = 0;

	for( IIcount=0x08000000 ; IIcount < MCU_Code_End ; IIcount++ )
	{
		TEMP_BUF = GDMCU_FMC_READ_DATA(IIcount);
		CHECKSUM = CHECKSUM + TEMP_BUF ;
	}
	CALIB_MEMORY_MAP.CHECKSUM_V = Swap_Bytes( CHECKSUM );
	CALIB_MEMORY_MAP.FW_VERSION = Swap_Bytes(MCU_FW_VERSION); 
    CALIB_MEMORY_MAP.CheckSum_EN = 0x00 ;
    GDMCU_Flash_Erase(FS_Cal0_P90);
	GDMCU_FMC_BytesWRITE_FUNCTION( FS_Cal0_P90  , &CALIB_MEMORY_MAP.VCC_SCALEM    , 128 );
}

void SFF8636_Table_GetData()
{
    GDMCU_FMC_READ_FUNCTION( FS_SFF8636_A0 , &SFF8636_A0[0] , 128 );
    GDMCU_FMC_READ_FUNCTION( FS_SFF8636_P0 , &SFF8636_A0[128] , 128 );
    GDMCU_FMC_READ_FUNCTION( FS_SFF8636_P0 , &SFF8636_PAGE0[0] , 128 );
    GDMCU_FMC_READ_FUNCTION( FS_SFF8636_P1 , &SFF8636_PAGE1[0] , 128 );
    GDMCU_FMC_READ_FUNCTION( FS_SFF8636_P2 , &SFF8636_PAGE2[0] , 128 );
    GDMCU_FMC_READ_FUNCTION( FS_SFF8636_P3 , &SFF8636_PAGE3[0] , 128 );
}

void Get_QDD_MSA_Optional_Control_Table()
{
	GDMCU_FMC_READ_FUNCTION(   FS_MSA_O_P91      , &CTEL_Table[0]   , 16 );
	GDMCU_FMC_READ_FUNCTION( ( FS_MSA_O_P91+16 ) , &Pre_Table[0]    , 16 );
	GDMCU_FMC_READ_FUNCTION( ( FS_MSA_O_P91+48 ) , &PW_LEVE1[0]     , 4  );
	GDMCU_FMC_READ_FUNCTION( ( FS_MSA_O_P91+32 ) , &OP_AMP_Table[0] , 16 );
	GDMCU_FMC_READ_FUNCTION( ( FS_MSA_O_P91+64 ) , &Post_Table[0]   , 16 );
}

void Device_Table_Get_Data()
{
	uint8_t Temp_Buffer[128];
	// Tx LDD AFSI_N74C4S
	GDMCU_FMC_READ_FUNCTION( FS_VCSEL_P82  , &MATA38435_TX1_TX4_MEMORY_MAP.CHIP_ID     , 128 );
	// Rx TIA MATA38434
	GDMCU_FMC_READ_FUNCTION( FS_TIA_P8A    , &MATA38434_RX1_RX4_TIA_MEMORY_MAP.CHIP_ID , 128 );
	// DSP Line-side & System-side CH0 - CH7
	GDMCU_FMC_READ_FUNCTION( FS_DSP_LS_P86        , &Temp_Buffer[0] , 128 );
	memcpy(  &BRCM_87540_DSP_LS_PHY0_MEMORY_MAP   , &Temp_Buffer[0] , 128 );
	GDMCU_FMC_READ_FUNCTION( FS_DSP_SS_P87        , &Temp_Buffer[0] , 128 );
	memcpy(  &BRCM_87540_DSP_SS_PHY0_MEMORY_MAP   , &Temp_Buffer[0] , 128 );
	GDMCU_FMC_READ_FUNCTION( FS_Cal0_P90          , &CALIB_MEMORY_MAP.VCC_SCALEM , 128 );
}

void Control_Byte_Setting()
{
    // Clear to 0x00 Byte2-81
	memset( &SFF8636_A0[2] , 0x00 , 80 ) ;
    // Clear to 0x00 Byte99-104
    memset( &SFF8636_A0[99] , 0x00 , 6 ) ;
    // Clear to 0x00 Byte119-127
    memset( &SFF8636_A0[119] , 0x00 , 9 ) ;
    // Bit1 : IntL
	SFF8636_A0[2]|=0x02;
    // Tx Disable
    SFF8636_A0[86] = 0x00 ;
	// CDR Control
	SFF8636_A0[98] = 0xFF ;
    //Get Control Default value from sram
    Bef_TxDIS_Power=SFF8636_A0[86];
    Bef_Tx_output=SFF8636_PAGE3[103];
}

void PAM4_PAGE30_Init()
{
	memset( &SFF8636_PAGE30[0] , 0x00 , 128 ) ;
	//Data Path power up
	SFF8636_PAGE30[0] = 0x00;
	// Application code 135 - 138
	SFF8636_PAGE30[7]  = 0x10;
	SFF8636_PAGE30[8]  = 0x10;
	SFF8636_PAGE30[9]  = 0x10;
	SFF8636_PAGE30[10] = 0x10;
    // MSA Byte191-195 ApSel Code 0001b
    SFF8636_PAGE30[63] = 0x04;
    SFF8636_PAGE30[64] = 0x0B;
    SFF8636_PAGE30[65] = 0x02;
    SFF8636_PAGE30[66] = 0x24;
    SFF8636_PAGE30[67] = 0x11;
    // MSA Byte195-199 Apsel code 0010b
    SFF8636_PAGE30[68] = 0x0F;
    SFF8636_PAGE30[69] = 0x03;
    SFF8636_PAGE30[70] = 0x24;
    SFF8636_PAGE30[71] = 0x11;
    // MSA Byte200-203 Apsel code 0011b
    SFF8636_PAGE30[68] = 0x00;
    SFF8636_PAGE30[69] = 0x00;
    SFF8636_PAGE30[70] = 0x00;
    SFF8636_PAGE30[71] = 0x00;
}

void PowerON_Table_Init()
{
	SFF8636_Table_GetData();
	Device_Table_Get_Data();

	Control_Byte_Setting();
	PAM4_PAGE30_Init();

    CALIB_MEMORY_MAP.FW_VERSION = Swap_Bytes(MCU_FW_VERSION); 
	memset(&CALIB_MEMORY_1_MAP, 0x00, 128);
}





