#include "gd32e501.h"
#include <stdio.h>

// GD Sample code Suggest
//#define I2C_SHORT_TIMEOUT 0x3ffff
//#define I2C_LONG_TIMEOUT  0x5fff
#define I2C_SHORT_TIMEOUT   0x200
#define I2C_LONG_TIMEOUT    0x200
#define I2Cx_TIMEOUT        0x200

typedef enum
{
  I2C_OK                                          = 0,
  I2C_FAIL                                        = 1
}I2C1_Status;

void Delay_I2C1(uint32_t i)
{
  while(i--);
}

void GPIO_Configuration_I2C1_PB34()
{  
    uint32_t GPIO_SDA;
    uint32_t GPIO_SCL;
    uint32_t GPIO_Pin_SDA,GPIO_Pin_SCL;
    uint32_t GPIO_AF_SDA,GPIO_AF_SCL;

    rcu_periph_reset_enable(RCU_I2C1RST);
    rcu_periph_reset_disable(RCU_I2C1RST);
    /* enable GPIOB clock */
    rcu_periph_clock_enable(RCU_GPIOB);
    /* enable BOARD_I2C APB1 clock */
    rcu_periph_clock_enable(RCU_I2C1);

    GPIO_SCL=GPIOB;
    GPIO_Pin_SCL=GPIO_PIN_3;
    GPIO_SDA=GPIOB;
    GPIO_Pin_SDA=GPIO_PIN_4;
    GPIO_AF_SCL=GPIO_AF_7;
    GPIO_AF_SDA=GPIO_AF_7;
    /* connect PB10 to I2C1_SCL */
    gpio_af_set(GPIO_SCL, GPIO_AF_SCL, GPIO_Pin_SCL);
    /* connect PB11 to I2C1_SDA */
    gpio_af_set(GPIO_SDA, GPIO_AF_SDA, GPIO_Pin_SDA);
    /* configure  I2C2 GPIO */
    gpio_mode_set(GPIO_SCL, GPIO_MODE_AF, GPIO_PUPD_PULLUP, GPIO_Pin_SCL);
    gpio_output_options_set(GPIO_SCL, GPIO_OTYPE_OD, GPIO_OSPEED_50MHZ, GPIO_Pin_SCL);
    gpio_mode_set(GPIO_SDA, GPIO_MODE_AF, GPIO_PUPD_PULLUP, GPIO_Pin_SDA);
    gpio_output_options_set(GPIO_SDA, GPIO_OTYPE_OD, GPIO_OSPEED_50MHZ, GPIO_Pin_SDA);
}

void Master_I2C1_PB34_Init(uint8_t Device_ADR)
{
    GPIO_Configuration_I2C1_PB34();
    /* configure I2C timing */
    i2c_timing_config(I2C1,0,0x9,0);
    i2c_master_clock_config(I2C1,0x78,0x78);    // 400 KHz
    //i2c_master_clock_config(I2C1,0x9C,0x9C);  // 100 KHz
    i2c_address_config(I2C1,0x82,I2C_ADDFORMAT_7BITS);
    /* send slave address to I2C bus */
    i2c_master_addressing(I2C1,Device_ADR,I2C_MASTER_TRANSMIT);
    /* enable I2Cx */
    i2c_enable(I2C1);
}

void Resume_IIC_I2C1( uint32_t Timeout ,uint8_t Device_ADR)
{
    uint32_t GPIO_SDA;
    uint32_t GPIO_SCL;
    uint32_t GPIO_Pin_SDA,GPIO_Pin_SCL;
    
    /* enable GPIOB clock */
    rcu_periph_clock_enable(RCU_GPIOB);
    /* enable BOARD_I2C APB1 clock */
    rcu_periph_clock_disable(RCU_I2C1);
    GPIO_SCL=GPIOB;
    GPIO_Pin_SCL=GPIO_PIN_3;
    GPIO_SDA=GPIOB;
    GPIO_Pin_SDA=GPIO_PIN_4;

    do
    {    
        gpio_mode_set(GPIO_SCL, GPIO_MODE_OUTPUT, GPIO_PUPD_PULLUP, GPIO_Pin_SCL);
        gpio_output_options_set(GPIO_SCL, GPIO_OTYPE_OD, GPIO_OSPEED_50MHZ, GPIO_Pin_SCL);
        gpio_mode_set(GPIO_SDA, GPIO_MODE_OUTPUT, GPIO_PUPD_PULLUP, GPIO_Pin_SDA);
        gpio_output_options_set(GPIO_SDA, GPIO_OTYPE_OD, GPIO_OSPEED_50MHZ, GPIO_Pin_SDA);  
        gpio_bit_reset(GPIO_SCL, GPIO_Pin_SCL);
        Delay_I2C1(20);
        gpio_bit_reset(GPIO_SDA, GPIO_Pin_SDA);
        Delay_I2C1(20);
        gpio_bit_set(GPIO_SCL, GPIO_Pin_SCL);
        Delay_I2C1(20);
        gpio_bit_set(GPIO_SDA, GPIO_Pin_SDA);
        Delay_I2C1(20);	
        if(Timeout-- == 0) return;
    }
    while((!gpio_input_bit_get(GPIO_SDA, GPIO_Pin_SDA))||(!gpio_input_bit_get(GPIO_SCL, GPIO_Pin_SCL)));
		
    Master_I2C1_PB34_Init(Device_ADR);
}

uint8_t Master_I2C1_ByteREAD_PB34(uint8_t SADR ,uint8_t Mem_REG )
{
    uint32_t I2Cx_Timeout = I2C_SHORT_TIMEOUT;
    uint8_t  READ_DATA = 0x00 ;
    uint8_t  Timeout_Flag = 1 ;
    
    i2c_master_addressing( I2C1 , SADR , I2C_MASTER_TRANSMIT );
    i2c_transfer_byte_number_config( I2C1 , 1 );
    while(i2c_flag_get( I2C1 , I2C_FLAG_I2CBSY))
    {
        if((I2Cx_Timeout--) == 0)
        {
            Resume_IIC_I2C1(I2C_LONG_TIMEOUT,SADR);
            break;
        }
    }    
    // step1 : I2C start + Address 
    i2c_start_on_bus(I2C1);
    // step2 : write data flag setting
    I2C_STAT(I2C1) |= I2C_STAT_TBE;
    
    I2Cx_Timeout = I2C_SHORT_TIMEOUT;
    while(!i2c_flag_get( I2C1 , I2C_FLAG_TBE))
    {
        if((I2Cx_Timeout--) == 0)
        {
            Resume_IIC_I2C1(I2C_LONG_TIMEOUT,SADR);
            break;
        }
    }    
    // Step3 : write memory reg. address
    i2c_data_transmit( I2C1 , Mem_REG );

    I2Cx_Timeout = I2C_SHORT_TIMEOUT;
    while(!i2c_flag_get( I2C1 , I2C_FLAG_TC))
    {
        if((I2Cx_Timeout--) == 0)
        {
            Resume_IIC_I2C1(I2C_LONG_TIMEOUT,SADR);
            break;
        }
    } 
    // Step4 : GD MCU I2C setting mode receive
	i2c_master_addressing( I2C1 , SADR , I2C_MASTER_RECEIVE);
    // Step5 : Read byte count setting
    i2c_transfer_byte_number_config( I2C1 , 1 );
    // Step6 : RESTART 
    i2c_start_on_bus(I2C1);    
    // step7 : Disable reload
    i2c_reload_disable(I2C1);
    // step8 : Enable I2C automatic end mode in master mode 
    i2c_automatic_end_enable(I2C1);
    // step9 : check flag read i2c temp buffer
    I2Cx_Timeout = I2C_SHORT_TIMEOUT;
    //Get data from I2C
    while(!i2c_flag_get( I2C1 , I2C_FLAG_RBNE))
    {
        //Timeout to break while loop
        if((I2Cx_Timeout--) == 0)
        {
            Timeout_Flag=0;
            break;
        }
    }
    //Get receive data if no timeout
    if(Timeout_Flag)
        READ_DATA = i2c_data_receive(I2C1);

    I2Cx_Timeout = I2C_SHORT_TIMEOUT;
    while(!i2c_flag_get(I2C1, I2C_FLAG_STPDET))
    {
        if((I2Cx_Timeout--) == 0)
        {
            Resume_IIC_I2C1(I2C_LONG_TIMEOUT,SADR);
            break;
        }
    }         
    // step10 : clear stop flag
    i2c_flag_clear(I2C1, I2C_FLAG_STPDET);
    i2c_automatic_end_disable(I2C1);
    //i2c_deinit(I2C1);
    return READ_DATA;
}

/*
uint8_t Master_I2C1_ByteREAD(uint8_t SADR ,uint8_t Mem_REG ,uint8_t *ReadBuffer)
{
    uint32_t I2Cx_Timeout = I2C_SHORT_TIMEOUT;
    
    i2c_master_addressing( I2C1 , SADR , I2C_MASTER_TRANSMIT );
    i2c_transfer_byte_number_config( I2C1 , 1 );
    while(i2c_flag_get( I2C1 , I2C_FLAG_I2CBSY))
    {
        if((I2Cx_Timeout--) == 0)
        {
            Resume_IIC_I2C1(I2C_LONG_TIMEOUT,SADR);
            return I2C_FAIL;
        }
    }    
    // step1 : I2C start + Address 
    i2c_start_on_bus(I2C1);
    // step2 : write data flag setting
    I2C_STAT(I2C1) |= I2C_STAT_TBE;
    
    I2Cx_Timeout = I2C_SHORT_TIMEOUT;
    while(!i2c_flag_get( I2C1 , I2C_FLAG_TBE))	
    {
        if((I2Cx_Timeout--) == 0)
        {
            Resume_IIC_I2C1(I2C_LONG_TIMEOUT,SADR);
            return I2C_FAIL;
        }
    }    
    // Step3 : write memory reg. address
    i2c_data_transmit( I2C1 , Mem_REG );

    I2Cx_Timeout = I2C_SHORT_TIMEOUT;
    while(!i2c_flag_get( I2C1 , I2C_FLAG_TC))	
    {
        if((I2Cx_Timeout--) == 0)
        {
            Resume_IIC_I2C1(I2C_LONG_TIMEOUT,SADR);
            return I2C_FAIL;
        }
    } 
    // Step4 : GD MCU I2C setting mode receive
	i2c_master_addressing( I2C1 , SADR , I2C_MASTER_RECEIVE); 
    // Step5 : Read byte count setting
    i2c_transfer_byte_number_config( I2C1 , 1 );  
    // Step6 : RESTART 
    i2c_start_on_bus(I2C1);    
    // step7 : Disable reload
    i2c_reload_disable(I2C1);
    // step8 : Enable I2C automatic end mode in master mode 
    i2c_automatic_end_enable(I2C1);
    // step9 : check flag read i2c temp buffer
    if(i2c_flag_get( I2C1 , I2C_FLAG_RBNE))
        *ReadBuffer = i2c_data_receive(I2C1);
    
    I2Cx_Timeout = I2C_SHORT_TIMEOUT;
    while(!i2c_flag_get(I2C1, I2C_FLAG_STPDET))
    {
        if((I2Cx_Timeout--) == 0)
        {
            Resume_IIC_I2C1(I2C_LONG_TIMEOUT,SADR);
            return I2C_FAIL;
        }
    }         
    // step10 : clear stop flag
    i2c_flag_clear(I2C1, I2C_FLAG_STPDET);  
    i2c_automatic_end_disable(I2C1);    
    return I2C_OK;
}
*/
void Master_I2C1_ByteWrite_PB34( uint8_t SADR , uint8_t Mem_REG , uint8_t Write_DATA)
{
    uint32_t I2Cx_Timeout = I2C_SHORT_TIMEOUT;
    // MCU I2C mode setting 
    i2c_master_addressing(I2C1, SADR, I2C_MASTER_TRANSMIT);
    i2c_transfer_byte_number_config(I2C1,2);
    i2c_enable(I2C1);
    // Check I2c BUS is free
    while(i2c_flag_get(I2C1, I2C_FLAG_I2CBSY))
    {
        if((I2Cx_Timeout--) == 0)
        {
            Resume_IIC_I2C1(I2C_LONG_TIMEOUT,SADR);
            break;
        }
    }
    // Step1 : Start + slave address 
    i2c_start_on_bus(I2C1);
    // step2 : write data flag setting
    I2C_STAT(I2C1) |= I2C_STAT_TI;
    
    I2Cx_Timeout = I2C_SHORT_TIMEOUT;
    while(!i2c_flag_get( I2C1 , I2C_STAT_TI))
    {
        if((I2Cx_Timeout--) == 0)
        {
            Resume_IIC_I2C1(I2C_LONG_TIMEOUT,SADR);
            break;
        }
    }
    // Step3 : write memory reg. address
    i2c_data_transmit( I2C1 , Mem_REG );
    I2Cx_Timeout = I2C_SHORT_TIMEOUT;
    while(!i2c_flag_get( I2C1 , I2C_FLAG_TI))
    {
        if((I2Cx_Timeout--) == 0)
        {
            Resume_IIC_I2C1(I2C_LONG_TIMEOUT,SADR);
            break;
        }
    }
    // Step4 : write data
    i2c_data_transmit( I2C1 , Write_DATA );
    I2Cx_Timeout = I2C_SHORT_TIMEOUT;
    while(!i2c_flag_get( I2C1 , I2C_FLAG_TC))
    {
        if((I2Cx_Timeout--) == 0)
        {
            Resume_IIC_I2C1(I2C_LONG_TIMEOUT,SADR);
            break;
        }
    }    
    // Step5 : Send a stop condition to I2C Bus
    i2c_stop_on_bus(I2C1);
    I2Cx_Timeout = I2C_SHORT_TIMEOUT;
    while(!i2c_flag_get(I2C1, I2C_FLAG_STPDET))
    {
        if((I2Cx_Timeout--) == 0)
        {
            Resume_IIC_I2C1(I2C_LONG_TIMEOUT,SADR);
            break;
        }
    }
    // step6 : clear sotp flag
    i2c_flag_clear(I2C1, I2C_FLAG_STPDET);
}
/*
uint8_t Master_I2C1_ByteWrite( uint8_t SADR , uint8_t Mem_REG , uint8_t Write_DATA)
{
    uint32_t I2Cx_Timeout = I2C_SHORT_TIMEOUT;
    // MCU I2C mode setting 
    i2c_master_addressing(I2C1, SADR, I2C_MASTER_TRANSMIT);
    i2c_transfer_byte_number_config(I2C1,2);
    i2c_enable(I2C1);
    // Check I2c BUS is free
    while(i2c_flag_get(I2C1, I2C_FLAG_I2CBSY))
    {
        if((I2Cx_Timeout--) == 0)
        {
            Resume_IIC_I2C1(I2C_LONG_TIMEOUT,SADR);
            return I2C_FAIL;
        }
    }
    // Step1 : Start + slave address 
    i2c_start_on_bus(I2C1);
    // step2 : write data flag setting
    I2C_STAT(I2C1) |= I2C_STAT_TI;
    
    I2Cx_Timeout = I2C_SHORT_TIMEOUT;
    while(!i2c_flag_get( I2C1 , I2C_STAT_TI))	
    {
        if((I2Cx_Timeout--) == 0)
        {
            Resume_IIC_I2C1(I2C_LONG_TIMEOUT,SADR);
            return I2C_FAIL;
        }
    }
    // Step3 : write memory reg. address
    i2c_data_transmit( I2C1 , Mem_REG );
    I2Cx_Timeout = I2C_SHORT_TIMEOUT;
    while(!i2c_flag_get( I2C1 , I2C_FLAG_TI))
    {
        if((I2Cx_Timeout--) == 0)
        {
            Resume_IIC_I2C1(I2C_LONG_TIMEOUT,SADR);
            return I2C_FAIL;
        }
    }
    // Step4 : write data
    i2c_data_transmit( I2C1 , Write_DATA );
    I2Cx_Timeout = I2C_SHORT_TIMEOUT;
    while(!i2c_flag_get( I2C1 , I2C_FLAG_TC))
    {
        if((I2Cx_Timeout--) == 0)
        {
            Resume_IIC_I2C1(I2C_LONG_TIMEOUT,SADR);
            return I2C_FAIL;
        }
    }    
    // Step5 : Send a stop condition to I2C Bus
    i2c_stop_on_bus(I2C1);
    I2Cx_Timeout = I2C_SHORT_TIMEOUT;
    while(!i2c_flag_get(I2C1, I2C_FLAG_STPDET))
    {
        if((I2Cx_Timeout--) == 0)
        {
            Resume_IIC_I2C1(I2C_LONG_TIMEOUT,SADR);
            return I2C_FAIL;
        }
    }
    // step6 : clear sotp flag
    i2c_flag_clear(I2C1, I2C_FLAG_STPDET);  
    return I2C_OK;        
}
*/



